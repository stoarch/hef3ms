﻿using System;
using System.Data;
using System.Windows;
using Hef3MS.Constants;
using Hef3MS.Data;
using Hef3MS.Sensors;
using Hef3MS_NET.SQLStrings;
using MySql.Data.MySqlClient;

namespace Hef3MS_NET.Data.Serialization
{
    public static class Hef3Data2Receiver
    {
        public static Hef3Data2 GetData2For(DateTime WorkDate)
        {
            Hef3Data2 data = new Hef3Data2();

            InitializeData(data);

            MySqlConnection conn = ConnectToServer();

            DataTable valueData = GetData2Table(WorkDate, conn);

            ConvertAndFillWith(data, valueData);

            return data;
        }

        private static TurbineData GetTurbineFor(Hef3Data2 data, ref Hef3ID id)
        {
            TurbineData turbine = data.Turbines[
                                    Hef3Data2_ID.GetTurbineIDFor(id.hef3id)
                                  ];
            return turbine;
        }

        private static void SetTurbineSensorValue(Hef3Data2 data, Hef3ID id)
        {
            GetTurbineFor(data, ref id)
                .GetSensorForId(id.hef3id, id.idValue_kid)
                    .Value = id.idValue;
        }

        private static bool IsTurbineParmID(int hef3id)
        {
            return hef3id >= Hef3Data2_ID.TURBINE_BASE;
        }

        private static bool IsBoilerParmID(int hef3id)
        {
            return ((hef3id >= Hef3Data2_ID.BOILER_BASE) && (hef3id < Hef3Data2_ID.TURBINE_BASE));
        }

        private static void CheckAndSetSensorValue(Hef3Data2 data, Hef3ID id)
        {
            if (IsBoilerParmID(id.hef3id))
            {
                SetBoilerSensorValue(data, id);
            }
            else if (IsTurbineParmID(id.hef3id))
            {
                SetTurbineSensorValue(data, id);
            }
        }

        private static void SetBoilerSensorValue(Hef3Data2 data, Hef3ID id)
        {
            if (null == data)
            {
                throw new ArgumentException("Data can not be null for setup sensor value");
            }

            BoilerData2 boiler = GetBoilerFor(data, ref id);

            if (null == boiler)
                throw new ArgumentException(String.Format("For id:{0} boiler is not found", id.hef3id));

            SingleValueSensor sensor =
                boiler.GetSensorForId(id.hef3id, id.idValue_kid);

            if (null == sensor)
                throw new ArgumentException(String.Format("For id:{0} and kid:{1} no sensors is found", id.hef3id, id.idValue_kid));

            sensor.Value = id.idValue;
        }

        private static BoilerData2 GetBoilerFor(Hef3Data2 data, ref Hef3ID id)
        {
            int boilerId = Hef3Data2_ID.GetBoilerIDFor(id.hef3id);

            if ((boilerId < 0) || (boilerId > data.Boilers.Count))
                throw new Exception(String.Format("Boiler id:{0} is out of range [0..{2}]", boilerId, data.Boilers.Count));

            BoilerData2 boiler = data.Boilers[
                                    boilerId
                                  ];
            if (null == boiler)
                throw new InvalidOperationException(String.Format("Boiler for id {0} is not found", id.hef3id));

            return boiler;
        }

        struct Hef3ID
        {
            public int hef3id;
            public float idValue;
            public DateTime dateInput;
            public int idValue_kid;
        }

        private static Hef3ID GetIDFromRow(DataRow row)
        {
            Hef3ID id;
            id.hef3id = row.Field<int>(1);
            id.idValue = (float)Math.Round(row.Field<float>(2), 2);
            id.dateInput = row.Field<DateTime>(3);
            id.idValue_kid = row.Field<int>(4);
            return id;
        }

        private static void ConvertAndFillWith(Hef3Data2 data, DataTable valueData)
        {
            try
            {
                foreach (DataRow row in valueData.Rows)
                {
                    CheckAndSetSensorValue(data, GetIDFromRow(row));
                }
            }
            catch (Exception e1)
            {
                //TODO: Make system logging
                MessageBox.Show("Извините за беспокойство.\nВозникла ошибка " + e1.Message + "\n" + e1.StackTrace);
            }
        }

        private static void FillDataTable(DateTime WorkDate, MySqlConnection conn, DataTable valueData)
        {
            string sqlHef3Id = SQLStringGenerator.Instance.GetIDValueSQL(WorkDate, WorkDate.AddDays(1));

            MySqlDataAdapter adapter = new MySqlDataAdapter(sqlHef3Id, conn);

            adapter.Fill(valueData);
        }

        private static DataTable GetData2Table(DateTime WorkDate, MySqlConnection conn)
        {
            try
            {
                DataTable valueData = new DataTable();

                FillDataTable(WorkDate, conn, valueData);

                return valueData;
            }
            catch (Exception e1)
            {
                //TODO: Make system logging
                MessageBox.Show("Извините за беспокойство.\nВозникла ошибка " + e1.Message + "\n" + e1.StackTrace);

                return null;
            }
        }

        private static MySqlConnection ConnectToServer()
        {
            try
            {
                //TODO: Move strings to settings

                MySqlConnection conn = new MySqlConnection("Server=192.168.254.125;Uid=hef3;Pwd=1;Database=Hef3Ms;");
                conn.Open();
                return conn;
            }
            catch (Exception e1)
            {
                //TODO: Make system logging
                MessageBox.Show("Извините за беспокойство.\nВозникла ошибка " + e1.Message + "\n" + e1.StackTrace);

                return null;
            }
        }

        private static void InitializeData(Hef3Data2 data)
        {
            try
            {
                InitializeBoilers(data);

                InitializeTurbines(data);
            }
            catch (Exception e1)
            {
                //TODO: Make system logging
                MessageBox.Show("Извините за беспокойство.\nВозникла ошибка " + e1.Message + "\n" + e1.StackTrace);
            }
        }

        private static void InitializeTurbines(Hef3Data2 data)
        {
            for (int i = 0; i < data.Turbines.Count; i++)
            {
                data.Turbines[i].Clear();
                data.Turbines[i].Number = i + 1;
            }
        }

        private static void InitializeBoilers(Hef3Data2 data)
        {
            for (int i = 0; i < data.Boilers.Count; i++)
            {
                data.Boilers[i].Clear();
                data.Boilers[i].Number = i + 1;
            }
        }
    }
}