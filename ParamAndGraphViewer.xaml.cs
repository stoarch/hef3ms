﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hef3MS_NET
{
	/// <summary>
	/// Interaction logic for ParamAndGraphViewer.xaml
	/// </summary>
	public partial class ParamAndGraphViewer : UserControl
	{
		public ParamAndGraphViewer()
		{
			this.InitializeComponent();
		}



        public String ParamCaption
        {
            get { return (String)GetValue(ParamCaptionProperty); }
            set { SetValue(ParamCaptionProperty, value); }
        }
        
        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }



        public List<DatePoint> GraphValues
        {
            get { return (List<DatePoint>)GetValue(GraphValuesProperty); }
            set { SetValue(GraphValuesProperty, value); }
        }

        public static readonly DependencyProperty GraphValuesProperty =
            DependencyProperty.Register("GraphValues", typeof(List<DatePoint>), typeof(ParamAndGraphViewer));

       

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(ParamAndGraphViewer), new UIPropertyMetadata(0));    

        
        public static readonly DependencyProperty ParamCaptionProperty =
            DependencyProperty.Register("ParamCaption", typeof(String), typeof(ParamAndGraphViewer), new UIPropertyMetadata(""));


	}
}