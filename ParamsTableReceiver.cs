﻿using System;
using System.Data;
using System.Windows;
using MySql.Data.MySqlClient;

namespace Hef3MS_NET.Data.Serialization
{
    public static class ParamsTableReceiver
    {
        //TODO: Refactor long list of params
        public delegate int GetSensorHandler(int number);

        //Hef3Data2_ID.TURBINE_BASE + Hef3Data2_ID.TURBINE_SHIFT * (number - 1) + paramId

        /// <summary>
        /// Loads the table from mysql db for specific sensor value for
        ///     specified period.
        /// </summary>
        /// <param name="paramId">The id of sensor.</param>
        /// <param name="subParamId">The id of sub sensor value.</param>
        /// <param name="startOfPeriod">The start of period for data receive.</param>
        /// <param name="endOfPeriod">The end of period for data receive.</param>
        /// <param name="number">The number of device (Turbine, Boiler).</param>
        /// <param name="getSensorID">The get sensor for number delegate .</param>
        /// <returns></returns>
        public static DataTable LoadTable(int paramId, int subParamId, System.DateTime? startOfPeriod, System.DateTime? endOfPeriod, int number, GetSensorHandler getSensorID)
        {
            //TODO: Move sql to Hef3MS.SQLStrings
            string sql =
                //"select id, hkid, value, DateInput, kid from " +
            "select DateInput, Value from " +
            "( " +
            "select * from hef3ms.hef3id " +
            "inner join ( " +
            "   SELECT max(hef3id.id) as mid " +
            "   FROM       hef3ms.idvalue idvalue " +
            "   INNER JOIN hef3ms.hef3id hef3id ON (hef3id.id = idvalue.hid) " +
            "   WHERE (hef3id.DateInput >= '{2:yyyy-MM-dd}') and (hef3id.DateInput < '{3:yyyy-MM-dd}') " +
            "         and (hef3id.hkid = {0} and idvalue.kid = {1}) " +
            "   group by DateInput, hkid " +
            "   ORDER BY hef3id.DateInput ASC, hef3id.hkid asc, idvalue.kid asc " +
            ") as mval on (mval.mid = hef3id.id) " +
            "inner join " +
            "( " +
            "   select hid, kid, value from hef3ms.idvalue " +
            ") as idval on (idval.hid = hef3id.id) " +
            " Where (kid = {1}) " +
            "order by hef3ms.hef3id.DateInput, hef3ms.hef3id.hkid, idval.kid " +
            " ) as valueData "
            ;

            int sensorId = getSensorID(number);

            using (MySqlConnection conn = ConnectToServer())
            {
                DataTable table = new DataTable();

                MySqlDataAdapter adapter = new MySqlDataAdapter(
                    new MySqlCommand(
                        String.Format(
                                sql,
                                sensorId,
                                subParamId,
                                startOfPeriod,
                                endOfPeriod
                            ),
                        conn
                        )
                    );

                adapter.Fill(table);

                return table;
            }
        }

        private static MySqlConnection ConnectToServer()
        {
            try
            {
                //TODO: Move strings to settings

                MySqlConnection conn = new MySqlConnection("Server=192.168.254.125;Uid=hef3;Pwd=1;Database=Hef3Ms;");
                conn.Open();
                return conn;
            }
            catch (Exception e1)
            {
                //TODO: Make system logging
                MessageBox.Show("Извините за беспокойство.\nВозникла ошибка " + e1.Message + "\n" + e1.StackTrace);

                return null;
            }
        }
    }
}