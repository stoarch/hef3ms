﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Hef3MS.Sensors.HefSensors;
using Hef3MS.Sensors;

namespace Hef3MS.Data
{

    public class BoilerData2
    {

        //TODO: Think about serialization using ISerializable
        public BoilerData2()
        {
            FeedWater = new FeedWaterSensor();
            Steam = new SteamSensor();

            ColdAirTemperature = new TemperatureSensor();
            AirBeforeHeaterTemperature = new TemperatureSensor();
            HotAirTemperature = new TemperatureSensor();

            WasteGasesTemperature = new TemperatureSensor();

            //TODO: Move magic strings to constant utility
            StartStopCount = new SingleValueSensor("StartStopCount");
            WorkingHoursCount = new SingleValueSensor("WorkingHoursCount");

            HeatGrossValue = new SingleValueSensor("HeatGrossValue");
        }

        public FeedWaterSensor FeedWater { get; set; }
        public SteamSensor Steam { get; set; }

        public TemperatureSensor AirBeforeHeaterTemperature { get; set; }
        public TemperatureSensor ColdAirTemperature { get; set; }
        public TemperatureSensor HotAirTemperature { get; set; }

        public TemperatureSensor WasteGasesTemperature { get; set; }

        public SingleValueSensor StartStopCount { get; set; }
        public SingleValueSensor WorkingHoursCount { get; set; }


        public SingleValueSensor HeatGrossValue { get; set; }
    }

}
