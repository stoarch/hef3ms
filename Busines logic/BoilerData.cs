﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Hef3MS_NET
{
    public class BoilerData : DependencyObject
    {
        private static BoilerData m_empty = null;

        public static BoilerData EmptyValue
        {
            get
            {
                if (null == m_empty)
                {
                    m_empty = new BoilerData();
                }

                return m_empty;
            }
        }

        public BoilerData()
        {
            GasData     = new BaseParameters();
            MasutData   = new BaseParameters();
            SteamData   = new BaseParameters();
            WaterData   = new BaseParameters();
        }

        #region Dependency properties 
        public static readonly DependencyProperty DateInputProperty =
                        DependencyProperty.Register("DateInput", typeof(DateTime), typeof(BoilerData));
        
        public static readonly DependencyProperty GasDataProperty =
                        DependencyProperty.Register("GasData", typeof(BaseParameters), typeof(BoilerData));

        public static readonly DependencyProperty MasutDataProperty =
                        DependencyProperty.Register("MasutData", typeof(BaseParameters), typeof(BoilerData));

        public static readonly DependencyProperty SteamDataProperty =
                        DependencyProperty.Register("SteamData", typeof(BaseParameters), typeof(BoilerData));

        public static readonly DependencyProperty WaterDataProperty =
                        DependencyProperty.Register("WaterData", typeof(BaseParameters), typeof(BoilerData));
        #endregion

        #region Public properties
        public int Id { get; set; }

        public DateTime DateInput 
        { 
            get
            {
                return (DateTime)GetValue(DateInputProperty);
            }
            set
            {
                SetValue(DateInputProperty, value);
            }
        }

        public BaseParameters GasData   
        {
            get
            {
                return (BaseParameters)GetValue(GasDataProperty);
            }
            private set
            {
                SetValue(GasDataProperty, value);
            }
        }

        public BaseParameters MasutData
        {
            get
            {
                return (BaseParameters)GetValue(MasutDataProperty);
            }
            private set
            {
                SetValue(MasutDataProperty, value);
            }
        }

        public BaseParameters SteamData
        {
            get
            {
                return (BaseParameters)GetValue(SteamDataProperty);
            }
            private set
            {
                SetValue(SteamDataProperty, value);
            }
        }

        public BaseParameters WaterData
        {
            get
            {
                return (BaseParameters)GetValue(WaterDataProperty);
            }
            private set
            {
                SetValue(WaterDataProperty, value);
            }
        }

        #endregion

        internal BaseParameters getParametersBy( ParmKind parmKindId )
        {
            //TODO: Think about more data kinds and flexibility to add new one 
            if (parmKindId == ParmKind.Gas)
            {
                return GasData;
            }
            else if (parmKindId == ParmKind.Masut)
            {
                return MasutData;
            }
            else if (parmKindId == ParmKind.Steam)
            {
                return SteamData;
            }
            else if (parmKindId == ParmKind.Water)
            {
                return WaterData;
            }
            else
            {
                throw new Exception("Invalid paramater kind passed");
            }
        }
    }
}
