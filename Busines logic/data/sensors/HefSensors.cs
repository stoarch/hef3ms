﻿using System;

namespace Hef3MS.Sensors.HefSensors
{
    public static class HEFSensorUID
    {
        public static Guid CirculateWater { get { return new Guid("f14df798-419f-4d4a-abeb-445902b14d0a"); } }

        public static Guid FeedWater { get { return new Guid("bece421b-c4d9-4de8-93b3-cde181b3ee5d"); } }

        public static Guid Steam { get { return new Guid("8a19e3ba-cf5d-4a58-9d04-571758ecacf2"); } }
    }

    public static class HefSensorsCaptions
    {
        public static string SteamEnthalpy1 { get { return "SteamEnthalpy1"; } }

        public static string SteamEnthalpy2 { get { return "SteamEnthalpy2"; } }

        public static string SteamEnthalpy3 { get { return "SteamEnthalpy3"; } }
    }

    public static class AirSensorCaption
    {
        public const string AirBeforeHeater = "AirBeforeHeater";
        public const string ColdAir = "ColdAir";
        public const string HotAir = "HotAir";
    }

    public class AirSensors : ComplexSensor
    {
        public AirSensors()
        {
            Sensors.Add(new TemperatureSensor(AirSensorCaption.AirBeforeHeater));
            Sensors.Add(new TemperatureSensor(AirSensorCaption.ColdAir));
            Sensors.Add(new TemperatureSensor(AirSensorCaption.HotAir));
        }

        public TemperatureSensor AirBeforeHeaterTemperature { get { return (TemperatureSensor)this[AirSensorCaption.AirBeforeHeater + SensorCaption.Temperature]; } }

        public TemperatureSensor ColdAirTemperature { get { return (TemperatureSensor)this[AirSensorCaption.ColdAir + SensorCaption.Temperature]; } }

        public TemperatureSensor HotAirTemperature { get { return (TemperatureSensor)this[AirSensorCaption.HotAir + SensorCaption.Temperature]; } }
    }

    public class FeedWaterSensor : MediumSensor
    {
        public FeedWaterSensor()
        {
            Caption = SensorCaption.FeedWater;
        }
    }

    public class SteamSensor : MediumSensor
    {
        public SteamSensor()
        {
            Caption = SensorCaption.Steam;

            Sensors.Add(new SingleValueSensor(HefSensorsCaptions.SteamEnthalpy1));
            Sensors.Add(new SingleValueSensor(HefSensorsCaptions.SteamEnthalpy2));
            Sensors.Add(new SingleValueSensor(HefSensorsCaptions.SteamEnthalpy3));
        }

        public SingleValueSensor SteamEnthalpy1 { get { return (SingleValueSensor)this[HefSensorsCaptions.SteamEnthalpy1]; } }

        public SingleValueSensor SteamEnthalpy2 { get { return (SingleValueSensor)this[HefSensorsCaptions.SteamEnthalpy2]; } }

        public SingleValueSensor SteamEnthalpy3 { get { return (SingleValueSensor)this[HefSensorsCaptions.SteamEnthalpy3]; } }
    }

    public class CirculateWaterSensor : ComplexSensor
    {
        public CirculateWaterSensor()
        {
            UID = HEFSensorUID.CirculateWater;

            Sensors.Add(new TemperatureSensor(SensorKindCaption.Incoming));
            Sensors.Add(new TemperatureSensor(SensorKindCaption.Outgoing));
        }

        public TemperatureSensor IncomingTemperature
        {
            get
            {
                return (TemperatureSensor)this[SensorKindCaption.Incoming + SensorParameterCaption.Temperature];
            }
        }

        public TemperatureSensor OutgoingTemperature
        {
            get
            {
                return (TemperatureSensor)this[SensorKindCaption.Outgoing + SensorParameterCaption.Temperature];
            }
        }
    }
}