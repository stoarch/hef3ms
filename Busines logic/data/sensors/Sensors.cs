﻿using System;
using System.Collections.Generic;
using System.Windows;

namespace Hef3MS.Sensors
{
    public class Sensor : DependencyObject
    {
        public Sensor()
        {
        }

        public virtual void Clear()
        {
        }

        public Sensor(string aCaption)
        {
            Caption = aCaption;
        }

        public string Caption
        {
            get { return (string)GetValue(CaptionProperty); }
            set { SetValue(CaptionProperty, value); }
        }

        public DateTime DateReceived
        {
            get { return (DateTime)GetValue(DateReceivedProperty); }
            set { SetValue(DateReceivedProperty, value); }
        }

        public Guid UID
        {
            get { return (Guid)GetValue(UIDProperty); }
            set { SetValue(UIDProperty, value); }
        }

        public static readonly DependencyProperty UIDProperty =
            DependencyProperty.Register("UID", typeof(Guid), typeof(Sensor));

        public static readonly DependencyProperty DateReceivedProperty =
            DependencyProperty.Register("DateReceived", typeof(DateTime), typeof(Sensor), new UIPropertyMetadata(DateTime.Now));

        public static readonly DependencyProperty CaptionProperty =
            DependencyProperty.Register("Caption", typeof(string), typeof(Sensor), new UIPropertyMetadata(""));
    }

    public static class SensorUID
    {
        public static Guid ComplexSensor { get { return new Guid("c493022a-a0e1-4303-86f6-3d755072e76a"); } }

        public static Guid SingleValueSensor { get { return new Guid("f14df798-419f-4d4a-abeb-445902b14d0a"); } }

        public static Guid Temperature { get { return new Guid("aa78314d-8ab2-4b14-b69b-cf24ba5409c1"); } }

        public static Guid Pressure { get { return new Guid("add4f19c-b8a0-46f3-ad1a-30b1ecd96af0"); } }

        public static Guid FlowRate { get { return new Guid("9fc3783a-9378-473e-ae70-3160aa8228ad"); } }

        public static Guid MediumSensor { get { return new Guid("f14df798-419f-4d4a-abeb-445902b14d0a"); } }
    }

    public class SingleValueSensor : Sensor
    {
        public SingleValueSensor()
        {
            UID = SensorUID.SingleValueSensor;
        }

        public SingleValueSensor(string caption)
            : base(caption)
        {
            UID = SensorUID.SingleValueSensor;
        }

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public override void Clear()
        {
            Value = 0;
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(Sensor), new UIPropertyMetadata(0.0));
    }

    public class ComplexSensor : Sensor
    {
        public ComplexSensor()
        {
            Sensors = new List<Sensor>();

            UID = SensorUID.ComplexSensor;
        }

        public List<Sensor> Sensors
        {
            get { return (List<Sensor>)GetValue(SensorsProperty); }
            set { SetValue(SensorsProperty, value); }
        }

        public static readonly DependencyProperty SensorsProperty =
            DependencyProperty.Register("Sensors", typeof(List<Sensor>), typeof(ComplexSensor));

        public Sensor this[Guid uid]
        {
            get
            {
                return Sensors.Find((sensor) => (sensor.UID.Equals(uid)));
            }
        }

        public Sensor this[string Caption]
        {
            get
            {
                return Sensors.Find((sensor) => (Caption == sensor.Caption));
            }
        }

        public override void Clear()
        {
            foreach (Sensor sensor in Sensors)
            {
                sensor.Clear();
            }
        }
    }

    public static class SensorParameterCaption
    {
        public static string Actual { get { return "Actual"; } }

        public static string ByDevice { get { return "ByDevice"; } }

        public static string Temperature { get { return "Temperature"; } }

        public static string Pressure { get { return "Pressure"; } }

        public static string FlowRate { get { return "FlowRate"; } }
    }

    public static class SensorCaption
    {
        public static string Temperature { get { return "Temperature"; } }

        public static string Pressure { get { return "Pressure"; } }

        public static string FlowRate { get { return "FlowRate"; } }

        public static string FeedWater { get { return "Feed water"; } }

        public static string Steam { get { return "Steam"; } }
    }

    public static class SensorKindCaption
    {
        public static string Incoming { get { return "Incoming"; } }

        public static string Outgoing { get { return "Outgoing"; } }
    }

    public class TemperatureSensor : SingleValueSensor
    {
        public TemperatureSensor()
            : base(SensorParameterCaption.Temperature)
        {
            UID = SensorUID.Temperature;
        }

        public TemperatureSensor(string header)
        {
            Caption = header + SensorParameterCaption.Temperature;
        }
    }

    public class PressureSensor : SingleValueSensor
    {
        public PressureSensor()
            : base(SensorParameterCaption.Pressure)
        {
            UID = SensorUID.Pressure;
        }
    }

    public class FlowRateSensor : ComplexSensor
    {
        public FlowRateSensor()
        {
            UID = SensorUID.FlowRate;

            Caption = SensorParameterCaption.FlowRate;

            Sensors.Add(new SingleValueSensor(SensorParameterCaption.Actual));
            Sensors.Add(new SingleValueSensor(SensorParameterCaption.ByDevice));
        }

        public new SingleValueSensor this[string Caption]
        {
            get
            {
                return (SingleValueSensor)Sensors.Find((value) => (value.Caption == Caption));
            }
        }

        public SingleValueSensor Actual
        {
            get
            {
                return this[SensorParameterCaption.Actual];
            }
        }

        public SingleValueSensor ByDevice
        {
            get
            {
                return this[SensorParameterCaption.ByDevice];
            }
        }
    }

    public class MediumSensor : ComplexSensor
    {
        public MediumSensor()
        {
            UID = SensorUID.MediumSensor;

            Sensors.Add(new TemperatureSensor());
            Sensors.Add(new PressureSensor());
            Sensors.Add(new FlowRateSensor());
        }

        public TemperatureSensor Temperature
        {
            get
            {
                return (TemperatureSensor)this[SensorParameterCaption.Temperature];
            }
        }

        public PressureSensor Pressure
        {
            get
            {
                return (PressureSensor)this[SensorParameterCaption.Pressure];
            }
        }

        public FlowRateSensor FlowRate
        {
            get
            {
                return (FlowRateSensor)this[SensorParameterCaption.FlowRate];
            }
        }
    }
}