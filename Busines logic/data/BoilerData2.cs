﻿using System.Windows;
using Hef3MS.Sensors;
using Hef3MS.Sensors.HefSensors;

namespace Hef3MS.Data
{
    public class BoilerData2 : DependencyObject
    {
        //TODO: Think about serialization using ISerializable
        public BoilerData2()
        {
            FeedWater = new FeedWaterSensor();
            Steam = new SteamSensor();

            Air = new AirSensors();

            WasteGasesTemperature = new TemperatureSensor();

            //TODO: Move magic strings to constant utility
            StartStopCount = new SingleValueSensor("StartStopCount");
            WorkingHoursCount = new SingleValueSensor("WorkingHoursCount");

            HeatGrossValue = new SingleValueSensor("HeatGrossValue");
        }

        #region Sensors

        public FeedWaterSensor FeedWater { get; set; }
        public SteamSensor Steam { get; set; }

        public AirSensors Air { get; set; }

        public TemperatureSensor WasteGasesTemperature { get; set; }

        public SingleValueSensor StartStopCount { get; private set; }
        public SingleValueSensor WorkingHoursCount { get; private set; }

        public SingleValueSensor HeatGrossValue { get; private set; }

        #endregion

        #region Properties

        public int Number
        {
            get { return (int)GetValue(NumberProperty); }
            set { SetValue(NumberProperty, value); }
        }

        public static readonly DependencyProperty NumberProperty =
            DependencyProperty.Register("Number", typeof(int), typeof(BoilerData2), new UIPropertyMetadata(0));

        #endregion

        internal void Clear()
        {
            FeedWater.Clear();
            Steam.Clear();
            Air.Clear();
            WasteGasesTemperature.Clear();
            StartStopCount.Clear();
            WorkingHoursCount.Clear();
            HeatGrossValue.Clear();
        }
    }
}