﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Hef3MS.Data
{
    

    public class Hef3Data2
    {
        private const int BOILERS_COUNT = 3;
        private const int TURBINES_COUNT = 2;

        public Hef3Data2()
        {
            InitBoilers();
            InitTurbines();
        }

        private void InitTurbines()
        {
            Turbines = new List<TurbineData>(TURBINES_COUNT);

            for (int i = 0; i < TURBINES_COUNT; i++)
            {
                Turbines.Add(new TurbineData());
            }
        }

        private void InitBoilers()
        {
            Boilers = new List<BoilerData2>(BOILERS_COUNT);

            for (int i = 0; i < BOILERS_COUNT; i++)
            {
                Boilers.Add(new BoilerData2());    
            }            
        }

        public List<BoilerData2> Boilers { get; private set; }

        public List<TurbineData> Turbines { get; private set; }
    }
}
