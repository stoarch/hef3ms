﻿using System.Windows;
using Hef3MS.Sensors;
using Hef3MS.Sensors.HefSensors;

namespace Hef3MS.Data
{
    public class TurbineData : DependencyObject
    {
        public TurbineData()
        {
            Temperature = new TemperatureSensor();
            Pressure = new PressureSensor();
            FlowRate = new FlowRateSensor();

            ExhaustSteam = new SteamSensor();
            POSteam = new SteamSensor();
            TOSteam = new SteamSensor();

            CirculateWater = new CirculateWaterSensor();

            //TODO: Move magic strings to tool class
            StartStopCount = new SingleValueSensor("StartStopCoount");
            WorkingHours = new SingleValueSensor("WorkingHours");
        }

        #region Sensors

        public TemperatureSensor Temperature { get; set; }

        public PressureSensor Pressure { get; set; }

        public FlowRateSensor FlowRate { get; set; }

        public SteamSensor ExhaustSteam { get; set; }

        public SteamSensor POSteam { get; set; } //factory needs

        public SteamSensor TOSteam { get; set; } //cogeneration picking

        public CirculateWaterSensor CirculateWater { get; set; }

        public SingleValueSensor WorkingHours { get; private set; }

        public SingleValueSensor StartStopCount { get; private set; }

        #endregion Sensors

        #region Properties

        public int Number
        {
            get { return (int)GetValue(NumberProperty); }
            set { SetValue(NumberProperty, value); }
        }

        public static readonly DependencyProperty NumberProperty =
            DependencyProperty.Register("Number", typeof(int), typeof(TurbineData), new UIPropertyMetadata(0));

        #endregion Properties

        internal void Clear()
        {
            Temperature.Clear();
            Pressure.Clear();
            FlowRate.Clear();

            ExhaustSteam.Clear();
            POSteam.Clear();
            TOSteam.Clear();

            CirculateWater.Clear();
            WorkingHours.Clear();
            StartStopCount.Clear();
        }
    }
}