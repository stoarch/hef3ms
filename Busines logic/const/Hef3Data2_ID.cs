﻿namespace Hef3MS.Constants
{
    /// <summary>
    ///     Serialization specific constants class. All constants define the id,
    /// stored into db "Hef3ID.ID".
    /// </summary>
    public static class Hef3Data2_ID
    {
        #region Boiler bases

        public const int BOILER_BASE = 10;
        public const int BOILER_SHIFT = 10;

        #endregion

        #region Turbine bases

        public const int TURBINE_BASE = 50;
        public const int TURBINE_SHIFT = 20;

        #endregion

        #region Boiler parms

        public const int BOILER_STEAM_SENSOR = 1;
        public const int BOILER_AIR_SENSORS = 2;
        public const int BOILER_GAS_SENSORS = 3;
        public const int BOILER_FEED_WATER_SENSORS = 4;
        public const int BOILER_MISC_PARAMS = 5;

        #endregion

        #region Turbine parms

        public const int TURBINE_TEMPERATURE_SENSOR = 1;
        public const int TURBINE_PRESSURE_SENSOR = 2;
        public const int TURBINE_FLOWRATE_SENSOR = 3;
        public const int TURBINE_EXHAUST_STEAM_SENSOR = 4;
        public const int TURBINE_PO_STEAM_SENSOR = 5;
        public const int TURBINE_TO_STEAM_SENSOR = 6;
        public const int TURBINE_CIRCULATE_WATER_SENSOR = 7;
        public const int TURBINE_MISC_PARAMS = 8;

        #endregion

        #region Common turbine parms

        public const int TEMPERATURE = 1;
        public const int PRESSURE = 1;

        public const int FLOWRATE_ACTUAL = 1;
        public const int FLOWRATE_BYDEVICE = 2;

        public const int WATER_TEMPERATURE_INCOMING = 1;
        public const int WATER_TEMPERATURE_OUTGOING = 2;

        #endregion

        #region Common boiler parms

        public const int STEAM_TEMPERATURE = 1;
        public const int STEAM_PRESSURE = 2;
        public const int STEAM_FLOWRATE_BYDEVICE = 3;
        public const int STEAM_FLOWRATE_ACTUAL = 4;
        public const int STEAM_ENTALPY1 = 5;
        public const int STEAM_ENTALPY2 = 6;
        public const int STEAM_ENTALPY3 = 7;

        public const int COLD_AIR_TEMPERATURE = 1;
        public const int AIR_BEFORE_HEATER_TEMPERATURE = 2;
        public const int HOT_AIR_TEMPERATURE = 3;

        public const int WASTE_GASES_TEMPERATURE = 1;

        public const int FEED_WATER_TEMPERATURE = 1;
        public const int FEED_WATER_PRESSURE = 2;
        public const int FEED_WATER_FLOWRATE_BYDEVICE = 3;
        public const int FEED_WATER_FLOWRATE_ACTUAL = 4;

        public const int WORKING_HOURS_COUNT = 1;
        public const int START_STOP_COUNT = 2;
        public const int HEAT_GROSS_VALUE = 3;

        #endregion

        public static int GetTurbineIDFor(int hef3id)
        {
            return ((hef3id - Hef3Data2_ID.TURBINE_BASE) / Hef3Data2_ID.TURBINE_SHIFT);
        }

        public static int GetBoilerIDFor(int hef3id)
        {
            return ((hef3id - Hef3Data2_ID.BOILER_BASE) / Hef3Data2_ID.BOILER_SHIFT);
        }

        public static int GetBoilerSensorID(int id)
        {
            if ((id < BOILER_BASE) || (id >= TURBINE_BASE))
                return -1;

            return id - (Hef3Data2_ID.BOILER_BASE + (Hef3Data2_ID.GetBoilerIDFor(id)) * Hef3Data2_ID.BOILER_SHIFT);
        }
    }
}