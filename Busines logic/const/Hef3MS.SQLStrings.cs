﻿using System;

namespace Hef3MS_NET.SQLStrings
{
    /// <summary>
    /// Provide SQL strings for database manipulation
    /// </summary>
    public class SQLStringGenerator
    {
        private static SQLStringGenerator m_instance = null;

        public static SQLStringGenerator Instance
        {
            get
            {
                if (null == m_instance)
                {
                    m_instance = new SQLStringGenerator();
                }

                return m_instance;
            }
        }

        private SQLStringGenerator()
        {
        }

        public String GetIDValueSQL(DateTime startDate, DateTime endDate)
        {
            string sql =
            "select id, hkid, value, DateInput, kid from " +
            "( " +
            "select * from hef3ms.hef3id " +
            "inner join ( " +
            "   SELECT max(hef3id.id) as mid " +
            "   FROM       hef3ms.idvalue idvalue " +
            "   INNER JOIN hef3ms.hef3id hef3id ON (hef3id.id = idvalue.hid) " +
            "   WHERE (hef3id.DateInput >= '{0:yyyy-MM-dd}') and (hef3id.DateInput < '{1:yyyy-MM-dd}') " +
            "   group by DateInput, hkid " +
            "   ORDER BY hef3id.DateInput ASC, hef3id.hkid asc, idvalue.kid asc " +
            ") as mval on (mval.mid = hef3id.id) " +
            "inner join " +
            "( " +
            "   select hid, kid, value from hef3ms.idvalue " +
            ") as idval on (idval.hid = hef3id.id) " +
            "order by hef3ms.hef3id.DateInput, hef3ms.hef3id.hkid, idval.kid " +
            " ) as valueData "
            ;

            return String.Format(sql, startDate, endDate);
        }
    }
}