﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Hef3MS_NET
{
    public class BaseParameters : DependencyObject
    {
        public BaseParameters()
        {

        }


        #region Static properties
        public static readonly DependencyProperty FlowRateProperty =
                        DependencyProperty.Register("FlowRate", typeof(double), typeof(BaseParameters), new PropertyMetadata(0.0));

        public static readonly DependencyProperty PressureProperty =
                        DependencyProperty.Register("Pressure", typeof(double), typeof(BaseParameters), new PropertyMetadata(0.0));

        public static readonly DependencyProperty TemperatureProperty =
                        DependencyProperty.Register("Temperature", typeof(double), typeof(BaseParameters), new PropertyMetadata(0.0));
        #endregion

        #region Properties
        public double FlowRate 
        {
            get
            {
                return (double)GetValue(FlowRateProperty);
            }
            set
            {
                SetValue(FlowRateProperty, value);
            }
        }
        
        public double Pressure 
        {
            get
            {
                return (double)GetValue(PressureProperty);
            }
            set
            {
                SetValue(PressureProperty, value);
            }
        }

        public double Temperature 
        {
            get
            {
                return (double)GetValue(TemperatureProperty);
            }
            set
            {
                SetValue(TemperatureProperty, value);
            }
        }
        #endregion

        internal void SetParmValue(ParmValueKind parmValueId, float value)
        {
            switch (parmValueId)
            {
                case ParmValueKind.FlowRate: FlowRate = value; break;
                case ParmValueKind.Pressure: Pressure = value; break;
                case ParmValueKind.Temperature: Temperature = value; break;
            }
        }
    }//BaseParameters
}//Hef3MS_NET
