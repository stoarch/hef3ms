﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hef3MS_NET
{
	public class Hef3Data2 : DependencyObject 
	{
        private static Hef3Data2 s_empty = new Hef3Data2();
        public static Hef3Data2 EmptyValue
        {
            get
            {
                return s_empty;
            }
        }

		public Hef3Data2()
		{
			Boilers = new BoilerList();
		}

        private BoilerList m_boilers = null;

		public BoilerList Boilers
		{
			get
			{
                return m_boilers;
			}
            private set
            {
                m_boilers = value;
            }
		}
	}
}