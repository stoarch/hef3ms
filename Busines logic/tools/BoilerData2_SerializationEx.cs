﻿using System;
using Hef3MS.Constants;
using Hef3MS.Data;
using Hef3MS.Sensors;

namespace Hef3MS_NET.Data.Serialization
{
    public static class BoilerData2_SerializationEx
    {
        /// <summary>
        /// Gets the sensor for id.
        /// </summary>
        /// <param name="boiler">The boiler for whom sensor is belongs</param>
        /// <param name="id">The id of sensor</param>
        /// <param name="subId">The sub id of sub-sensor params</param>
        /// <returns></returns>
        public static SingleValueSensor GetSensorForId(this BoilerData2 boiler, int id, int subId)
        {
            if (null == boiler)
                throw new ArgumentException("Boiler can not be null to receive sensor for it!");

            int sensorId = Hef3Data2_ID.GetBoilerSensorID(id);

            //TODO: Think to identify by UID, stored in db field (thru dictionary)
            //TODO: Replace switch with polymorphism
            switch (sensorId)
            {
                case Hef3Data2_ID.BOILER_AIR_SENSORS: return boiler.Air.GetSensorById(subId);
                case Hef3Data2_ID.BOILER_FEED_WATER_SENSORS: return boiler.FeedWater.GetSensorById(subId);
                case Hef3Data2_ID.BOILER_MISC_PARAMS: return GetBoilerMiscParamsSensor(boiler, subId);
                case Hef3Data2_ID.BOILER_GAS_SENSORS: return boiler.WasteGasesTemperature;
                case Hef3Data2_ID.BOILER_STEAM_SENSOR: return boiler.Steam.GetSensorById(subId);

                default:
                    throw new ArgumentException(String.Format("For id:{0} and sensorId:{1} no sensors is found!", id, sensorId));
            }
        }

        private static SingleValueSensor GetBoilerMiscParamsSensor(BoilerData2 boiler, int subId)
        {
            //TODO: Refactor switch to polymorphism
            switch (subId)
            {
                case Hef3Data2_ID.HEAT_GROSS_VALUE: return boiler.HeatGrossValue;
                case Hef3Data2_ID.START_STOP_COUNT: return boiler.StartStopCount;
                case Hef3Data2_ID.WORKING_HOURS_COUNT: return boiler.WorkingHoursCount;

                default:
                    break;
            }

            return null;
        }
    }
}