﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Hef3MS.Sensors;
using Hef3MS.Data;
using Hef3MS.Constants;
using Hef3MS.Sensors.HefSensors;

using Hef3MS_NET.Data.Serialization;

namespace Hef3MS_NET.Data.Serialization
{
    public static class TurbineData_SerializationEx
    {
        public static SingleValueSensor GetSensorForId(this TurbineData turbine, int id, int subId)
        {
            int sensorId = id - ( Hef3Data2_ID.TURBINE_BASE + (Hef3Data2_ID.GetTurbineIDFor(id) ) * Hef3Data2_ID.TURBINE_SHIFT);

            switch (sensorId)
            {
                case Hef3Data2_ID.TURBINE_CIRCULATE_WATER_SENSOR:
                    return turbine.CirculateWater.GetSensorByID(subId);
                case Hef3Data2_ID.TURBINE_EXHAUST_STEAM_SENSOR:
                    return turbine.ExhaustSteam.GetSensorById(subId);
                case Hef3Data2_ID.TURBINE_FLOWRATE_SENSOR:
                    return turbine.FlowRate.GetSensorById(subId);
                case Hef3Data2_ID.TURBINE_MISC_PARAMS:
                    return turbine.GetMiscParamsSensor(subId);
                case Hef3Data2_ID.TURBINE_PO_STEAM_SENSOR:
                    return turbine.POSteam.GetSensorById(subId);
                case Hef3Data2_ID.TURBINE_PRESSURE_SENSOR:
                    return turbine.Pressure;
                case Hef3Data2_ID.TURBINE_TEMPERATURE_SENSOR:
                    return turbine.Temperature;
                case Hef3Data2_ID.TURBINE_TO_STEAM_SENSOR:
                    return turbine.TOSteam.GetSensorById(subId);

                default:
                    throw new ArgumentException(String.Format("For subid:{0} turbine sensor is not defined", subId ));
            }
        }

        private static SingleValueSensor GetMiscParamsSensor(this TurbineData turbine, int subId)
        {
            //TODO: Refactor switch to polymorphism
            switch (subId)
            {
                case Hef3Data2_ID.START_STOP_COUNT: return turbine.StartStopCount;
                case Hef3Data2_ID.WORKING_HOURS_COUNT: return turbine.WorkingHours;

                default:
                    break;
            }

            return null;
        }



    }
}
