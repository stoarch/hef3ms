﻿using System;
using Hef3MS.Constants;
using Hef3MS.Sensors;
using Hef3MS.Sensors.HefSensors;

namespace Hef3MS_NET.Data.Serialization
{
    public static class SensorSerializationTool
    {
        public static SingleValueSensor GetSensorByID(this CirculateWaterSensor sensor, int id)
        {
            switch (id)
            {
                case Hef3Data2_ID.WATER_TEMPERATURE_INCOMING: return sensor.IncomingTemperature;
                case Hef3Data2_ID.WATER_TEMPERATURE_OUTGOING: return sensor.OutgoingTemperature;

                default:
                    throw new ArgumentException(String.Format("No steam sensor can be found for subId:{0}", id));
            }
        }

        public static SingleValueSensor GetSensorById(this AirSensors air, int subId)
        {
            //TODO: Refactor switch to polymorphism
            switch (subId)
            {
                case Hef3Data2_ID.AIR_BEFORE_HEATER_TEMPERATURE: return air.AirBeforeHeaterTemperature;
                case Hef3Data2_ID.COLD_AIR_TEMPERATURE: return air.ColdAirTemperature;
                case Hef3Data2_ID.HOT_AIR_TEMPERATURE: return air.HotAirTemperature;
            }
            return null;
        }

        public static SingleValueSensor GetSensorById(this FeedWaterSensor sensor, int subId)
        {
            //TODO: Refactor switch to polymorphism
            switch (subId)
            {
                case Hef3Data2_ID.FEED_WATER_FLOWRATE_ACTUAL: return sensor.FlowRate.Actual;
                case Hef3Data2_ID.FEED_WATER_FLOWRATE_BYDEVICE: return sensor.FlowRate.ByDevice;
                case Hef3Data2_ID.FEED_WATER_PRESSURE: return sensor.Pressure;
                case Hef3Data2_ID.FEED_WATER_TEMPERATURE: return sensor.Temperature;

                default:
                    throw new ArgumentException(String.Format("No steam sensor can be found for subId:{0}", subId));
            }
        }

        public static SingleValueSensor GetSensorById(this SteamSensor steam, int subId)
        {
            if (null == steam)
                throw new ArgumentException("Steam sensor is null!");

            //TODO: Refactor switch to polymorphism
            switch (subId)
            {
                case Hef3Data2_ID.STEAM_TEMPERATURE:
                    {
                        if (null != steam.Temperature)
                            return steam.Temperature;
                        else
                            throw new ArgumentException("Steam temperature sensor is undefined");
                    }

                case Hef3Data2_ID.STEAM_PRESSURE:
                    {
                        if (null != steam.Pressure)
                            return steam.Pressure;
                        else
                            throw new ArgumentException("Steam pressure sensor is undefined");
                    }

                case Hef3Data2_ID.STEAM_FLOWRATE_ACTUAL:
                    {
                        if (null != steam.FlowRate)
                        {
                            if (null != steam.FlowRate.Actual)
                                return steam.FlowRate.Actual;
                            else
                                throw new ArgumentException("Steam FlowRate.Actual sensor is undefined");
                        }
                        else
                            throw new ArgumentException("Steam FlowRate sensor is undefined");
                    }

                case Hef3Data2_ID.STEAM_FLOWRATE_BYDEVICE:
                    if (null != steam.FlowRate)
                    {
                        if (null != steam.FlowRate.ByDevice)
                            return steam.FlowRate.ByDevice;
                        else
                            throw new ArgumentException("Steam FlowRate.ByDevice sensor is undefined");
                    }
                    else
                        throw new ArgumentException("Steam FlowRate sensor is undefined");

                case Hef3Data2_ID.STEAM_ENTALPY1:
                    {
                        if (null != steam.SteamEnthalpy1)
                            return steam.SteamEnthalpy1;
                        else
                            throw new ArgumentException("Steam enthalpy1 sensor is undefined");
                    }

                case Hef3Data2_ID.STEAM_ENTALPY2:
                    {
                        if (null != steam.SteamEnthalpy2)
                            return steam.SteamEnthalpy2;
                        else
                            throw new ArgumentException("Steam enthalpy2 sensor is undefined");
                    }

                case Hef3Data2_ID.STEAM_ENTALPY3:
                    {
                        if (null != steam.SteamEnthalpy3)
                            return steam.SteamEnthalpy3;
                        else
                            throw new ArgumentException("Steam enthalpy3 sensor is undefined");
                    }

                default:
                    throw new ArgumentException(String.Format("No steam sensor can be found for subId:{0}", subId));
            }
        }

        public static SingleValueSensor GetSensorById(this FlowRateSensor sensor, int id)
        {
            switch (id)
            {
                case Hef3Data2_ID.FLOWRATE_ACTUAL:
                    return sensor.Actual;
                case Hef3Data2_ID.FLOWRATE_BYDEVICE:
                    return sensor.ByDevice;
                default:
                    throw new ArgumentException(String.Format("No steam sensor can be found for subId:{0}", id));
            }
        }
    }
}