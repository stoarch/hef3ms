﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Hef3MS.Data;

namespace Hef3MS_NET
{
    /// <summary>
    /// Interaction logic for AnimatedBoilerControl.xaml
    /// </summary>
    public partial class AnimatedBoilerControl : UserControl
    {
        public static DependencyProperty DataProperty =
                        DependencyProperty.Register("Data", typeof(BoilerData2), typeof(AnimatedBoilerControl));

        public AnimatedBoilerControl()
        {
            InitializeComponent();
        }

        public BoilerData2 Data
        {
            get
            {
                return (BoilerData2)GetValue(DataProperty);
            }
            set
            {
                SetValue(DataProperty, value);
            }
        }

        private void UserControl_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
        {
        }

        private void Storyboard_Completed(object sender, System.EventArgs e)
        {
        }

        private void UserControl_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
        {
        }

        private void closeButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        }

        private void detailsButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            NavigationService.GetNavigationService(this).Navigate(new BoilerControlsPage());
        }
    }
}