﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Hef3MS.Data;

namespace Hef3MS_NET
{
	/// <summary>
	/// Interaction logic for SmallBoilerControl.xaml
	/// </summary>
	public partial class SmallBoilerControl : UserControl
	{
        public static readonly DependencyProperty GasParmsVisibleProperty =
                    DependencyProperty.Register("GasParmsVisible", typeof(bool), typeof(SmallBoilerControl), new PropertyMetadata(true));

        public static readonly DependencyProperty MasutParmsVisibleProperty =
                    DependencyProperty.Register("MasutParmsVisible", typeof(bool), typeof(SmallBoilerControl), new PropertyMetadata(true));
        
        public static readonly DependencyProperty SteamParmsVisibleProperty =
                    DependencyProperty.Register("SteamParmsVisible", typeof(bool), typeof(SmallBoilerControl), new PropertyMetadata(true));

        public static readonly DependencyProperty WaterParmsVisibleProperty =
                    DependencyProperty.Register("WaterParmsVisible", typeof(bool), typeof(SmallBoilerControl), new PropertyMetadata(true));


        public static readonly DependencyProperty DataProperty =
                    DependencyProperty.Register("Data", typeof(BoilerData2), typeof(SmallBoilerControl));

        public BoilerData2 Data
        {
            get
            {
                return (BoilerData2)GetValue(DataProperty);
            }
            set
            {
                SetValue(DataProperty, value);
            }
        }

        public bool GasParmsVisible
        {
            get
            {
                return (bool)GetValue(GasParmsVisibleProperty);
            }
            set
            {
                SetValue(GasParmsVisibleProperty, value);
            }
        }

        public bool MasutParmsVisible
        {
            get
            {
                return (bool)GetValue(MasutParmsVisibleProperty);
            }
            set
            {
                SetValue(MasutParmsVisibleProperty, value);
            }
        }

        public bool SteamParmsVisible
        {
            get
            {
                return (bool)GetValue(SteamParmsVisibleProperty);
            }
            set
            {
                SetValue(SteamParmsVisibleProperty, value);
            }
        }


        public bool WaterParmsVisible
        {
            get
            {
                return (bool)GetValue(WaterParmsVisibleProperty);
            }
            set
            {
                SetValue(WaterParmsVisibleProperty, value);
            }
        }

        public SmallBoilerControl()
		{
			this.InitializeComponent();

        }

        public bool IsDrawingDisplayed
        {
            get { return (bool)GetValue(IsDrawingDisplayedProperty); }
            set { SetValue(IsDrawingDisplayedProperty, value); }
        }

        // Using a DependencyProperty as the backing store for IsDrawingDisplayed.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsDrawingDisplayedProperty =
            DependencyProperty.Register("IsDrawingDisplayed", typeof(bool), typeof(SmallBoilerControl), new UIPropertyMetadata(true));


	}
}