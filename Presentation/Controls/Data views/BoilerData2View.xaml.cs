﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Hef3MS.Data;

namespace Hef3MS_NET
{
	/// <summary>
	/// Interaction logic for BoilerData2View.xaml
	/// </summary>
	public partial class BoilerData2View : UserControl
	{
		public BoilerData2View()
		{
			this.InitializeComponent();
		}

        public BoilerData2 Data
        {
            get { return (BoilerData2)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }



        public static readonly DependencyProperty DataProperty = 
            DependencyProperty.Register("Data", typeof(BoilerData2), typeof(BoilerData2View)
    );


	}
}