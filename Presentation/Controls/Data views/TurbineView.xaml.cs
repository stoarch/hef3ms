﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Hef3MS.Data;

namespace Hef3MS_NET
{
	/// <summary>
	/// Interaction logic for TurbineView.xaml
	/// </summary>
	public partial class TurbineView : UserControl
	{
		public TurbineView()
		{
			this.InitializeComponent();
		}



        public TurbineData Data
        {
            get { return (TurbineData)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        public static readonly DependencyProperty DataProperty = 
            DependencyProperty.Register("Data", typeof(TurbineData), typeof(TurbineView));
	}
}