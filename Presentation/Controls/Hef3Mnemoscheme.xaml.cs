﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Hef3MS.Data;
using System.Windows.Media.Animation;

namespace Hef3MS_NET
{
	/// <summary>
	/// Interaction logic for Hef3Mnemoscheme.xaml
	/// </summary>
	public partial class Hef3Mnemoscheme : UserControl
	{
        

        public static readonly DependencyProperty DataProperty =
                            DependencyProperty.Register("Data", typeof(Hef3Data2), typeof(Hef3Mnemoscheme));

        public Hef3Data2 Data
        {
            get
            {
                return (Hef3Data2)GetValue(DataProperty);
            }
            set
            {
                SetValue(DataProperty, value);
            }
        }

		public Hef3Mnemoscheme()
		{
            Data = new Hef3Data2();

			this.InitializeComponent();
		}

		private void coldAllDirectionsPipe_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
		{
			
		}

		private void AnimatedBoilerControl_MouseLeftButtonUp(object sender, System.Windows.Input.MouseButtonEventArgs e)
		{
			
		}

        public void SetDefParms()
        {
        }

//TODO: Refactor duplicates in events
        private void Boiler2View_MouseEnter(object sender, MouseEventArgs e)
        {
            DisplayBoilerDataViewWith(this.Data.Boilers[0]);
        }

        private void DisplayBoilerDataViewWith(BoilerData2 data)
        {
            boilerDataView.Data = data;

            DoubleAnimation opacityAnim = new DoubleAnimation(1, TimeSpan.FromMilliseconds(80));

            boilerDataView.BeginAnimation(Canvas.OpacityProperty, opacityAnim);

            boilerDataView.Visibility = Visibility.Visible;
        }

        private void HideBoilerDataView()
        {
            DoubleAnimation opacityVaneAnim = new DoubleAnimation(0, TimeSpan.FromMilliseconds(30));

            opacityVaneAnim.Completed += new EventHandler(
                delegate(object senderA, EventArgs e1)
                {
                    boilerDataView.Visibility = System.Windows.Visibility.Collapsed;
                }
            );

            boilerDataView.BeginAnimation(Canvas.OpacityProperty, opacityVaneAnim);
        }

        private void Boiler2View_MouseLeave(object sender, MouseEventArgs e)
        {
            DoubleAnimation opacityVanishAnim = new DoubleAnimation(0, TimeSpan.FromMilliseconds(30));
            opacityVanishAnim.Completed += new EventHandler(
                delegate(object senderA, EventArgs e1)
                {
                    boilerDataView.Visibility = Visibility.Collapsed;
                }
            );

            boilerDataView.BeginAnimation(Canvas.OpacityProperty, opacityVanishAnim);            
        }

        private void boiler2_MouseEnter(object sender, MouseEventArgs e)
        {
            DisplayBoilerDataViewWith(this.Data.Boilers[1]);
        }

        private void boiler2_MouseLeave(object sender, MouseEventArgs e)
        {
            HideBoilerDataView();
        }

        private void boiler3_MouseEnter(object sender, MouseEventArgs e)
        {
            DisplayBoilerDataViewWith(this.Data.Boilers[2]);
        }

        private void boiler3_MouseLeave(object sender, MouseEventArgs e)
        {
            HideBoilerDataView();
        }

        private void turbine1_MouseEnter(object sender, MouseEventArgs e)
        {
            DisplayTurbineDataViewWith(this.Data.Turbines[0]);
        }

        private void turbine1_MouseLeave(object sender, MouseEventArgs e)
        {
            HideTurbineDataView();
        }

        private void turbine2_MouseEnter(object sender, MouseEventArgs e)
        {
            DisplayTurbineDataViewWith(this.Data.Turbines[1]);
        }

        private void DisplayTurbineDataViewWith(TurbineData data)
        {
            turbineDataView.Data = data;

            DoubleAnimation opacityAnim = new DoubleAnimation(1, TimeSpan.FromMilliseconds(80));

            turbineDataView.BeginAnimation(Canvas.OpacityProperty, opacityAnim);

            turbineDataView.Visibility = Visibility.Visible;
        }

        private void turbine2_MouseLeave(object sender, MouseEventArgs e)
        {
            HideTurbineDataView();
        }

        private void HideTurbineDataView()
        {
            DoubleAnimation opacityOutAnim = new DoubleAnimation(0, TimeSpan.FromMilliseconds(30));

            opacityOutAnim.Completed += new EventHandler(
                delegate(Object senderA, EventArgs args)
                {
                    turbineDataView.Visibility = Visibility.Collapsed;
                }
            );

            turbineDataView.BeginAnimation(Canvas.OpacityProperty, opacityOutAnim);
        }
	}
}