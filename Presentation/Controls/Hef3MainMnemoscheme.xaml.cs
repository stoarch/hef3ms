﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

using System.Data.Common;
using System.Data.SqlClient;
using System.Data.OleDb;

using MySql.Data.MySqlClient;
using System.Data;

using Hef3MS.Data;
using Hef3MS_NET.Data.Serialization;

using Hef3MS_NET.SQLStrings;
using Hef3MS.Sensors;
using Hef3MS.Constants;

namespace Hef3MS_NET
{
	public partial class Hef3MainMnemoscheme
	{
		public Hef3MainMnemoscheme()
		{
			this.InitializeComponent();
		}

        public static readonly DependencyProperty WorkDateProperty;

        static Hef3MainMnemoscheme()
        {
            FrameworkPropertyMetadata metadata = new FrameworkPropertyMetadata( DateTime.Now );
            metadata.Journal = true;

            WorkDateProperty = DependencyProperty.Register("WorkDate", typeof(DateTime), typeof(Hef3MainMnemoscheme), metadata, null);
        }
        
        public DateTime WorkDate
        {
            get
            {
                return (DateTime)GetValue(WorkDateProperty);
            }
            set
            {
                SetValue(WorkDateProperty, value);
            }
        }

		private void getDataButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
		}

        private void DisplayData2(Hef3Data2 h3d)
        {
            throw new NotImplementedException();
        }

		
		private void DisplayData( Hef3Data2 data )
		{
            _hef3ms.Data = data;
            _hef3ms.SetDefParms();
		}


	}//Hef3MainMnemoscheme
}