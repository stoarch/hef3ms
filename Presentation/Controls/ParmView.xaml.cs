﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hef3MS_NET
{
	/// <summary>
	/// Interaction logic for ParmView.xaml
	/// </summary>
	public partial class ParmView : UserControl
	{
        public static readonly DependencyProperty ParamValueProperty = 
                DependencyProperty.Register( "ParamValue", typeof(double), typeof(ParmView),
                    new FrameworkPropertyMetadata( new PropertyChangedCallback(OnParamValueChanged))
                );

		public static readonly DependencyProperty ParamCaptionProperty =
				DependencyProperty.Register( "ParamCaption", typeof(string), typeof(ParmView),
					new FrameworkPropertyMetadata( new PropertyChangedCallback(OnParamCaptionChanged) )
				);
		
		private static void OnParamCaptionChanged( DependencyObject sender, 
								DependencyPropertyChangedEventArgs e )
		{
			ParmView pv = (ParmView) sender;
			
			pv.paramCaptionView.Inlines.Clear();
			pv.paramCaptionView.Inlines.Add( new Run( e.NewValue.ToString()));
		}

        private static void OnParamValueChanged( DependencyObject sender,
                                DependencyPropertyChangedEventArgs e )
        {
            ParmView pv = (ParmView) sender;

            pv.parmValueView.Inlines.Clear();
            pv.parmValueView.Inlines.Add( new Run( e.NewValue.ToString()));
        }
		
        public double ParamValue
        {
            get
            {
                return (double)GetValue(ParamValueProperty);
            }
            set
            {
                SetValue( ParamValueProperty, value );
            }
        }

		public string ParamCaption
		{
			get
			{
				return	(string)GetValue(ParamCaptionProperty);
			}
			set
			{
				SetValue( ParamCaptionProperty, value );
			}
		}
		
		public ParmView()
		{
			this.InitializeComponent();
		}

		private void paramCaptionView_MouseEnter(object sender, System.Windows.Input.MouseEventArgs e)
		{
			this.paramCaptionView.FontSize = 12;
		}

		private void paramCaptionView_MouseLeave(object sender, System.Windows.Input.MouseEventArgs e)
		{
			this.paramCaptionView.FontSize = 8;
		}
	}
}