﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Hef3MS_NET
{
	
	/// <summary>
	/// Interaction logic for ParmsColView.xaml
	/// </summary>
	public partial class ParmsColView : UserControl
	{
		public ParmsColView()
		{
            Data = new BoilerData();
            
            this.InitializeComponent();
		}

		private void gasButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            ParmState = ParmsViewState.Gas;
		}

		private void steamButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            ParmState = ParmsViewState.Steam;
		}

		private void masutButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
            ParmState = ParmsViewState.Masut;
		}

		private void waterButton_Click(object sender, System.Windows.RoutedEventArgs e)
		{
			ParmState = ParmsViewState.Water;
		}



        public ParmsViewState ParmState
        {
            get 
            { 
                return (ParmsViewState)GetValue(StateProperty); 
            }
            set 
            { 
                SetValue(StateProperty, value);
                
                UpdateBindings();
                UpdateUI();
            }
        }

        private Binding GetBinding(string path, string elementName)
        {
            Binding binding = new Binding(path);
            binding.ElementName = elementName;

            return binding;
        }

        private void UpdateBindings()
        {
            if (null == this.Data)
                return;

            switch (ParmState)
            {
                case ParmsViewState.Gas:
                    {                        
                        temperatureView.SetBinding(TextBlock.TextProperty, GetBinding("Data.GasData.Temperature", "UserControl"));
                        pressureView.SetBinding(TextBlock.TextProperty, GetBinding("Data.GasData.Pressure", "UserControl"));
                        flowRateView.SetBinding(TextBlock.TextProperty, GetBinding("Data.GasData.FlowRate", "UserControl"));

                        break;
                    }

                case ParmsViewState.Masut:
                    {
                        temperatureView.SetBinding(TextBlock.TextProperty, GetBinding("Data.MasutData.Temperature", "UserControl"));
                        pressureView.SetBinding(TextBlock.TextProperty, GetBinding("Data.MasutData.Pressure", "UserControl"));
                        flowRateView.SetBinding(TextBlock.TextProperty, GetBinding("Data.MasutData.FlowRate","UserControl"));

                        break;
                    }

                case ParmsViewState.Steam:
                    {
                        temperatureView.SetBinding(TextBlock.TextProperty, GetBinding("Data.SteamData.Temperature", "UserControl"));
                        pressureView.SetBinding(TextBlock.TextProperty, GetBinding("Data.SteamData.Pressure", "UserControl"));
                        flowRateView.SetBinding(TextBlock.TextProperty, GetBinding("Data.SteamData.FlowRate","UserControl"));

                        break;
                    }

                case ParmsViewState.Water:
                    {
                        temperatureView.SetBinding(TextBlock.TextProperty, GetBinding("Data.WaterData.Temperature", "UserControl"));
                        pressureView.SetBinding(TextBlock.TextProperty, GetBinding("Data.WaterData.Pressure", "UserControl"));
                        flowRateView.SetBinding(TextBlock.TextProperty, GetBinding("Data.WaterData.FlowRate", "UserControl"));

                        break;
                    }
            }
        }

        private void UpdateUI()
        {
            ParmCaption = GetParmCaption(ParmState);
            StateColor = GetStateColor(ParmState);
        }

        private Color GetStateColor(ParmsViewState ParmState)
        {
            switch (ParmState)
            {
                case ParmsViewState.Gas: return Color.FromArgb(0xFF, 0xE7, 0xED, 0x5B);//#FFE7ED5B;
                case ParmsViewState.Masut: return Color.FromArgb(0xFF, 0x45, 0xAF, 0xB8);//#FF45AFB8
                case ParmsViewState.Steam: return Color.FromArgb(0xFF, 0x62, 0xAD, 0xA6);//#FF62ADA6
                case ParmsViewState.Water: return Color.FromArgb(0xFF, 0x53, 0x63, 0xE3);//#FF3343B2
            }
            return Color.FromArgb(0xFF,0xFF,0xFF,0xFF);
        }

        private string GetParmCaption(ParmsViewState State)
        {
            switch (State)
            {
                case ParmsViewState.Gas: return "Газ";
                case ParmsViewState.Masut: return "Мазут";
                case ParmsViewState.Steam: return "Пар";
                case ParmsViewState.Water: return "Вода";
            }
            return "Нет";
        }

        public static readonly DependencyProperty StateProperty =
            DependencyProperty.Register("ParmState", typeof(ParmsViewState), typeof(ParmsColView), new UIPropertyMetadata(ParmsViewState.Gas));


        public String ParmCaption
        {
            get { return (String)GetValue(ParmCaptionProperty); }
            set { SetValue(ParmCaptionProperty, value); }
        }
      
        public static readonly DependencyProperty ParmCaptionProperty =
            DependencyProperty.Register("ParmCaption", typeof(String), typeof(ParmsColView), new UIPropertyMetadata("Газ"));


        public Color StateColor
        {
            get { return (Color)GetValue(StateColorProperty); }
            set 
            { 
                SetValue(StateColorProperty, value);                       
            }
        }

        public static readonly DependencyProperty StateColorProperty =
            DependencyProperty.Register("StateColor", typeof(Color), typeof(ParmsColView), new UIPropertyMetadata(Color.FromArgb( 0xFF, 0xD4, 0xDA, 0x50 )));


        public Visibility IsModiControlsVisible
        {
            get { return (Visibility)GetValue(IsModiControlsVisibleProperty); }
            set { SetValue(IsModiControlsVisibleProperty, value); }
        }

        public static readonly DependencyProperty IsModiControlsVisibleProperty =
            DependencyProperty.Register("IsModiControlsVisible", typeof(Visibility), typeof(ParmsColView), new UIPropertyMetadata(Visibility.Visible));

        public BoilerData Data
        {
            get { return (BoilerData)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(BoilerData), typeof(ParmsColView));

        private void temperatureDetailsButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	GetDetailsParamsFor( 
                    ParmValueKind.Temperature, 
                    DateTime.Now.AddMonths(-1), 
                    DateTime.Now, 
                    Data.Id
                );
        }

        private void GetDetailsParamsFor(ParmValueKind kind,DateTime startDate,DateTime endDate,int id)
        {
            Hef3Data2 data = GetDataFor(startDate, endDate);

            foreach (BoilerData item in data.Boilers)
            {

            }
        }

        private Hef3Data2 GetDataFor(DateTime startDate, DateTime endDate)
        {
            Hef3DataReceiveCommand cmd = new Hef3DataReceiveCommand(startDate, endDate);

            if (cmd.Execute())
                return cmd.Data;
            else
                return null;
        }      

        private void flowRateDetailsButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
        }

        private void pressureDetailsButton_Click(object sender, System.Windows.RoutedEventArgs e)
        {
        	// TODO: Add event handler implementation here.
        }


	}
}