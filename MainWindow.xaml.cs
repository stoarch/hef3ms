﻿#define DEBUG
#undef DEBUG

using System;
using System.Windows;
using System.Windows.Input;
using Hef3MS.Data;
using Hef3MS_NET.Data.Serialization;

namespace Hef3MS_NET
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private Hef3Data2 m_data = null;

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            try
            {
                //TODO: Make -1 not -2
#if DEBUG
                Hef3Data2 data = GetData2For(DateTime.Now.AddDays(-2)); //because they input data for yesterday and our time is one day further
#else
                Hef3Data2 data = GetData2For(DateTime.Now.AddDays(-1)); //because they input data for yesterday
#endif

                mnemoschemeView.Data = data;
                m_data = data;
            }
            catch (Exception e1)
            {
                //TODO: Make system logging
                MessageBox.Show("Извините за беспокойство.\nВозникла ошибка " + e1.Message + "\n" + e.OriginalSource);
            }
        }

        //TODO: Make async loading
        private Hef3Data2 GetData2For(DateTime WorkDate)
        {
            return Hef3Data2Receiver.GetData2For(WorkDate);
        }

        private void mnemoschemeTab_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ShowMnemoscheme();
        }

        private void ShowMnemoscheme()
        {
            this.turbineView.Visibility = Visibility.Collapsed;
            this.boilerView.Visibility = Visibility.Collapsed;
            this.mnemoscheme.Visibility = Visibility.Visible;
        }

        private void boiler1Tab_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ShowBoiler(1);
        }

        private void ShowBoiler(int boilerNo)
        {
            if (null != m_data)
            {
                this.boilerDetailView.Data = m_data.Boilers[boilerNo - 1];
            }

            this.turbineView.Visibility = Visibility.Collapsed;
            this.mnemoscheme.Visibility = Visibility.Collapsed;
            this.boilerView.Visibility = Visibility.Visible;
        }

        private void boiler2Tab_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ShowBoiler(2);
        }

        private void boiler3Tab_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ShowBoiler(3);
        }

        private void turbine1Tab_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ShowTurbine(1);
        }

        private void ShowTurbine(int turbineNo)
        {
            if (null != m_data)
            {
                this.turbineDetailView.Data = m_data.Turbines[turbineNo - 1];
                turbineDetailView.DataContext = null;
            }

            this.boilerView.Visibility = Visibility.Collapsed;
            this.mnemoscheme.Visibility = Visibility.Collapsed;
            this.turbineView.Visibility = Visibility.Visible;
        }

        private void turbine2Tab_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            ShowTurbine(2);
        }
    }
}