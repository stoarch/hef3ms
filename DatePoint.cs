﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;

namespace Hef3MS_NET
{
    public class DatePoint : DependencyObject
    {


        public DateTime Aquired
        {
            get { return (DateTime)GetValue(AquiredProperty); }
            set { SetValue(AquiredProperty, value); }
        }



        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(DatePoint), new UIPropertyMetadata(0.0F));

        

        public static readonly DependencyProperty AquiredProperty =
            DependencyProperty.Register("Aquired", typeof(DateTime), typeof(DatePoint), new UIPropertyMetadata(0));


    }
}
