﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Windows.Interactivity;

//                <i:Interaction.Behaviors>
//                    <behave:ExpandingMouseOver ScaleFactor="2" />
//                </i:Interaction.Behaviors>

namespace Hef3MS_NET
{
	
    public class ExpandingMouseOver : Behavior<UIElement>
    {
        private Storyboard _storyboard;
        private DoubleAnimation _daScaleX, _daScaleY;
        private ScaleTransform _scScale;

        public static readonly DependencyProperty ScaleFactorProperty = DependencyProperty.Register("ScaleFactor", typeof(double), typeof(ExpandingMouseOver), new PropertyMetadata(1.5));

        public double ScaleFactor
        {
            get
            {
                return (double)GetValue(ScaleFactorProperty);
            }
            set
            {
                SetValue(ScaleFactorProperty, value);
            }
        }

        protected override void OnAttached()
        {
            base.OnAttached();

            AssociatedObject.MouseEnter += new MouseEventHandler(OnMouseEnter);
            AssociatedObject.MouseLeave += new MouseEventHandler(OnMouseLeave);
            AssociatedObject.MouseLeftButtonDown += new MouseButtonEventHandler(OnLeftButtonDown);

            _scScale = new ScaleTransform();
            AssociatedObject.RenderTransformOrigin = new Point(0.5, 0.5);



            if (AssociatedObject.RenderTransform == null)
            {
                AssociatedObject.RenderTransform = _scScale;
            }
            else
            {
                if (AssociatedObject.RenderTransform is TransformGroup)
                {
                    ((TransformGroup)AssociatedObject.RenderTransform).Children.Add(_scScale);
                }
                else
                {
                    TransformGroup group = new TransformGroup();
                    group.Children.Add(AssociatedObject.RenderTransform);
                    group.Children.Add(_scScale);
                    AssociatedObject.RenderTransform = group;
                }
            }



            _daScaleX = new DoubleAnimation();
            _daScaleY = new DoubleAnimation();
            _daScaleX.Duration = _daScaleY.Duration = new Duration(new TimeSpan(0, 0, 0, 0, 150));

            Storyboard.SetTarget(_daScaleX, _scScale);
            Storyboard.SetTarget(_daScaleY, _scScale);
            Storyboard.SetTargetProperty(_daScaleX, new PropertyPath("ScaleX"));
            Storyboard.SetTargetProperty(_daScaleY, new PropertyPath("ScaleY"));

            _storyboard = new Storyboard();
            _storyboard.Children.Add(_daScaleX);
            _storyboard.Children.Add(_daScaleY);

        }



        protected override void OnDetaching()
        {
            base.OnDetaching();

            AssociatedObject.MouseEnter -= new MouseEventHandler(OnMouseEnter);
            AssociatedObject.MouseLeave -= new MouseEventHandler(OnMouseLeave);
            AssociatedObject.MouseLeftButtonDown -= new MouseButtonEventHandler(OnLeftButtonDown);
        }

        void OnMouseEnter(object sender, MouseEventArgs e)
        {
            _daScaleX.To = _daScaleY.To = ScaleFactor;
            _storyboard.Begin();
        }

        void OnMouseLeave(object sender, MouseEventArgs e)
        {
            _daScaleX.To = _daScaleY.To = 1.0;
            _storyboard.Begin();
        }
        void OnLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            _daScaleX.To = _daScaleY.To = 1.0;
            _storyboard.Begin();
        }

    }
}