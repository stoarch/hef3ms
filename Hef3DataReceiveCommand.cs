﻿using System;
using MySql.Data.MySqlClient;
using System.Data;
using System.Collections.Generic;
using Hef3MS_NET.SQLStrings;

namespace Hef3MS_NET
{
    #region PARM ENUMS
    /// <summary>
    /// Kind of parameter to work with:
    /// </summary>
    public enum ParmKind
    {
        /// <summary>
        /// Not known kind
        /// </summary>
        Unknown,
        /// <summary>
        /// Gas data
        /// </summary>
        Gas,
        /// <summary>
        /// Masut data
        /// </summary>
        Masut,
        /// <summary>
        /// Steam data
        /// </summary>
        Steam,
        /// <summary>
        /// Water data
        /// </summary>
        Water
    }

    public enum ParmValueKind
    {
        Unknown,
        FlowRate,
        Pressure,
        Temperature
    }
    #endregion
    public class Hef3DataParm
    {
        public int KindId { get; set; }
        public float Value { get; set; }
        public DateTime DateTime { get; set; }

        public DateTime Acquired { get; set; }
        public Hef3Data2 Owner { get; set; }
    }


    /// <summary>
    /// Convert datatable to hef3data
    /// </summary>
    public class DataTableToHef3DataConverter
    {
        public DataTable Source { get; set; }
        public Hef3Data2 Destination { get; private set; }

        public bool Execute()
        {
            Destination = ConvertDataTableToHef3Data(Source);
            return true;
        }

        private Hef3Data2 ConvertDataTableToHef3Data(DataTable data)
        {
            Hef3Data2 h3data = new Hef3Data2();

            foreach (DataRow row in data.Rows)
            {                
                Hef3DataParm parm = GetParmFor(row, h3data);

                FillBoiler(
                        GetBoilerFor(
                            parm
                           ),
                        parm
                       );
            }//foreach

            return h3data;
        }

        #region Parm Value Consts
        private const int FLOWRATE_ID = 0;
        private const int TEMPERATURE_ID = 1;
        private const int PRESSURE_ID = 2;
        #endregion

        #region Database constants
        private const int DATETIME_ROW = 2;
        private const int KIND_ID_ROW = 4;
        private const int VALUE_ROW = 1;
        #endregion

        private Hef3DataParm GetParmFor(DataRow row, Hef3Data2 data)
        {
            Hef3DataParm parm = new Hef3DataParm();

            parm.Acquired = (DateTime)row[DATETIME_ROW];
            parm.Owner = data;

            parm.KindId = (int)row[KIND_ID_ROW]; //Boiler value kind identifier
            parm.Value = (float)row[VALUE_ROW];
            parm.DateTime = (DateTime)row[DATETIME_ROW];

            return parm;
        }

        private Dictionary<Int32, BoilerData> m_boilers = new Dictionary<Int32, BoilerData>();

        private BoilerData GetBoilerFor(Hef3DataParm parms)
        {
            Int32 boilerId = GetBoilerNoFor(parms.KindId);

            if (m_boilers.ContainsKey(boilerId))
            {
                BoilerData boiler = null;

                if (m_boilers.TryGetValue(boilerId, out boiler))
                    return boiler;
                else
                    return null;
            }
            else
            {
                BoilerData boiler = new BoilerData();

                //TODO: Move out initialization
                boiler.Id = boilerId;
                boiler.DateInput = parms.Acquired;

                parms.Owner.Boilers.Add(boiler);

                m_boilers.Add(boilerId, boiler);

                return boiler;
            }
        }

        #region BOILER RANGE CONSTS
        private const int MIN_BOILER_VALUE_ID = 50;
        private const int MAX_BOILER_VALUE_ID = 85;

        private const int ALL_BOILER_VALUE_RANGE = MAX_BOILER_VALUE_ID - MIN_BOILER_VALUE_ID + 1;
        private const int BOILERS_COUNT = 3;
        private const int BOILER_VALUE_RANGE = ALL_BOILER_VALUE_RANGE / BOILERS_COUNT;
        #endregion

        #region BOILER PARAMS
        private const int GasFlowRate = 0;
        private const int GasTemperature = 1;
        private const int GasPressure = 2;

        private const int MasutFlowRate = 3;
        private const int MasutTemperature = 4;
        private const int MasutPressure = 5;

        private const int SteamFlowRate = 6;
        private const int SteamTemperature = 7;
        private const int SteamPressure = 8;

        private const int WaterFlowRate = 9;
        private const int WaterTemperature = 10;
        private const int WaterPressure = 11;
        #endregion

        #region BOILER1 Parameters id
        private const int BOILER1_GasFlowRate = 50;
        private const int BOILER1_GasTemperature = 51;
        private const int BOILER1_GasPressure = 52;

        private const int BOILER1_MasutFlowRate = 53;
        private const int BOILER1_MasutTemperature = 54;
        private const int BOILER1_MasutPressure = 55;

        private const int BOILER1_SteamFlowRate = 56;
        private const int BOILER1_SteamTemperature = 57;
        private const int BOILER1_SteamPressure = 58;

        private const int BOILER1_WaterFlowRate = 59;
        private const int BOILER1_WaterTemperature = 60;
        private const int BOILER1_WaterPressure = 61;
        #endregion

        #region BOILER2 Parameters id
        private const int BOILER2_GasFlowRate = 62;
        private const int BOILER2_GasTemperature = 63;
        private const int BOILER2_GasPressure = 64;

        private const int BOILER2_MasutFlowRate = 65;
        private const int BOILER2_MasutTemperature = 66;
        private const int BOILER2_MasutPressure = 67;

        private const int BOILER2_SteamFlowRate = 68;
        private const int BOILER2_SteamTemperature = 69;
        private const int BOILER2_SteamPressure = 70;

        private const int BOILER2_WaterFlowRate = 71;
        private const int BOILER2_WaterTemperature = 72;
        private const int BOILER2_WaterPressure = 73;
        #endregion

        #region BOILER3 Parameters id
        private const int BOILER3_GasFlowRate = 74;
        private const int BOILER3_GasTemperature = 75;
        private const int BOILER3_GasPressure = 76;

        private const int BOILER3_MasutFlowRate = 77;
        private const int BOILER3_MasutTemperature = 78;
        private const int BOILER3_MasutPressure = 79;

        private const int BOILER3_SteamFlowRate = 80;
        private const int BOILER3_SteamTemperature = 81;
        private const int BOILER3_SteamPressure = 82;

        private const int BOILER3_WaterFlowRate = 83;
        private const int BOILER3_WaterTemperature = 84;
        private const int BOILER3_WaterPressure = 85;
        #endregion

        private int GetBoilerNoFor(int valueKindId)
        {
            if ((valueKindId < MIN_BOILER_VALUE_ID) || (valueKindId > MAX_BOILER_VALUE_ID))
                throw new InvalidExpressionException("valueKindId is not valid");

            return ((((valueKindId - MIN_BOILER_VALUE_ID) / BOILER_VALUE_RANGE) + 1) % (BOILERS_COUNT + 1));
        }//GetDataFor

        private int GetBoilerParmId(int valueKindId)
        {
            int parmId = 0;

            if (valueKindId >= BOILER3_GasFlowRate)
                parmId = valueKindId - BOILER3_GasFlowRate;
            else if (valueKindId >= BOILER2_GasFlowRate)
                parmId = valueKindId - BOILER2_GasFlowRate;
            else if (valueKindId >= BOILER1_GasFlowRate)
                parmId = valueKindId - BOILER1_GasFlowRate;

            return parmId;
        }

            
        private void FillBoiler(BoilerData data, Hef3DataParm parm)
        {
            data.getParametersBy(
                    GetParmKindFor(parm.KindId)
                  )
                  .SetParmValue(
                     GetParmValueKindFor(parm.KindId),
                     parm.Value
                    );
        }

        private const int PARM_VALUE_COUNT = 3;

        private ParmValueKind GetParmValueKindFor(int valueKindId)
        {
            return (ParmValueKind)(GetBoilerParmId(valueKindId) % PARM_VALUE_COUNT + 1);
        }

        private ParmKind GetParmKindFor(int valueKindId)
        {
            return (ParmKind)(GetBoilerParmId(valueKindId) / PARM_VALUE_COUNT + 1);
        }
    }


    /// <summary>
    /// Retrieve hef3data from mysql database
    /// </summary>
    public class Hef3DataTableRetreiver
    {
        public DataTable Data { get;set; }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }


        public bool Execute()
        {
            Data = GetHef3DataTable();

            return true;
        }
        
        private String GetFormatedSQL()
        {
            return SQLStringGenerator.Instance.GetIDValueSQL(StartDate, EndDate);
        }

        private DataTable GetHef3DataTable()
        {
            //TODO: Make async execution
            //TODO: Move these strings to config file
            //TODO: Make connection and data adapter in factory
            //TODO: Unbind from MySQL database
            //TODO: Read connection setup from settings file

            MySqlConnection conn = new MySqlConnection("Server=192.168.254.125;Uid=hef3;Pwd=1;Database=Hef3Ms;");
            conn.Open();

            MySqlDataAdapter da = new MySqlDataAdapter(GetFormatedSQL(), conn);

            DataTable data = new DataTable();

            da.Fill(data);

            return data;
        }
    }


    /// <summary>
    /// Provide mechanism to receive hef3data from mysql database
    /// </summary>
    public class Hef3DataReceiveCommand
    {
       
        public Hef3DataReceiveCommand(DateTime startDate, DateTime endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
        }

        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public bool Execute()
        {
            m_data = GetHef3Data();
            return true;
        }

        public Hef3Data2 Data 
        { 
            get
            {
                return m_data;
            } 

            private set
            {        
            }
        }

        private Hef3Data2 m_data = new Hef3Data2();

        private Hef3Data2 GetHef3Data()
        {
            DataTableToHef3DataConverter converter = new DataTableToHef3DataConverter();

            converter.Source = GetHef3DataTable();

            if (converter.Execute())
            {
                return converter.Destination;
            }

            //TODO: Make NULL Object for Hef3Data
            return null;
        }

        private DataTable GetHef3DataTable()
        {
            Hef3DataTableRetreiver retriever = new Hef3DataTableRetreiver();

            retriever.StartDate = StartDate;
            retriever.EndDate = EndDate;

            if (retriever.Execute())
            {
                return retriever.Data;
            }

            return null;
        }
    }
}
