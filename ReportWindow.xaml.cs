﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Printing;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Xps;
using C1.WPF.C1Chart;
using Hef3MS.Data;
using Hef3MS.Sensors;

namespace Hef3MS_NET
{
    public enum ReportKind
    {
        Unknown,
        BoilerReport,
        TurbineReport
    }

    /// <summary>
    /// Interaction logic for ReportWindow.xaml
    /// </summary>
    public partial class ReportWindow : Window
    {
        public ReportWindow()
        {
            this.InitializeComponent();
        }

        private void InitializeTable()
        {
            FlowDocument docReport = GetDocument();

            Table valuesTable = (Table)docReport.FindName("ValuesTable");
            TableRowGroup group = valuesTable.RowGroups[0];

            if (group.Rows.Count > 1)
                group.Rows.RemoveRange(1, group.Rows.Count - 1);

            foreach (var item in Data)
            {
                TableRow row = new TableRow();
                row.Cells.Add(new TableCell(new Paragraph(new Run(item.DateReceived.ToShortDateString()))));
                row.Cells.Add(new TableCell(new Paragraph(new Run(item.Value.ToString()))));

                valuesTable.RowGroups[0].Rows.Add(row);
            }
        }

        private void printReportButton_Click(object sender, RoutedEventArgs e)
        {
            PrintReport();
        }

        private void PrintReport()
        {
            PrintDialog pd = new PrintDialog();

            if (pd.ShowDialog() == true)
            {
                XpsDocumentWriter xps_writer = PrintQueue.CreateXpsDocumentWriter(pd.PrintQueue);

                FlowDocument docReport = GetDocument();

                if (null == docReport)
                    throw new InvalidOperationException("docReport is not found in XAML!");

                IDocumentPaginatorSource paginator_source =
                    (IDocumentPaginatorSource)docReport;

                xps_writer.Write(paginator_source.DocumentPaginator);
            }
        }

        private FlowDocument GetDocument()
        {
            FlowDocument docReport = ((FlowDocument)FindResource("docReport"));
            return docReport;
        }

        private List<SingleValueSensor> m_data = null;

        public List<SingleValueSensor> Data
        {
            get
            {
                return m_data;
            }

            set
            {
                m_data = value;

                InitializeTable();
                InitializeChart();
            }
        }

        private void InitializeChart()
        {
            ChartData _defaultData = new ChartData();

            DataSeries ds1 = new DataSeries() { Label = "s1" };

            ds1.ValuesSource =
               (
               from item in Data
               select item.Value
               ).ToArray()
            ;

            ds1.ConnectionStroke = new SolidColorBrush(Colors.Blue);

            _defaultData.Children.Add(ds1);

            C1Chart chart = (C1Chart)GetDocument().FindName("chart");

            chart.ChartType = ChartType.Line;
            chart.Data = _defaultData;
        }

        private BoilerData2 m_boilerData = null;

        public BoilerData2 BoilerData
        {
            get
            {
                return m_boilerData;
            }
            set
            {
                m_boilerData = value;

                GetActiveDocBoilerView().Data = value;

                UpdateCaptions();
            }
        }

        private void UpdateCaptions()
        {
            FlowDocument doc = GetDocument();

            int number = 0;

            switch (ReportKind)
            {
                case ReportKind.Unknown:
                    break;

                case ReportKind.BoilerReport:
                    {
                        number = BoilerData.Number;

                        break;
                    }

                case ReportKind.TurbineReport:
                    {
                        number = TurbineData.Number;

                        break;
                    }

                default:
                    break;
            }

            Run boilerCaption = (Run)doc.FindName("boilerCaption");
            boilerCaption.Text = String.Format("Данные по {2} {0} на {1:dd-MM-yyyy}", number, CurrentDate, GetCurrentObjectCaption());

            Run tableCaption = (Run)doc.FindName("tableCaption");
            tableCaption.Text = String.Format("Таблица {0} на период {1:dd-MM-yyyy} - {2:dd-MM-yyyy}", ParamCaption, StartDate, EndDate);
        }

        private string GetCurrentObjectCaption()
        {
            switch (ReportKind)
            {
                case ReportKind.Unknown:
                    return "(не известный)";

                case ReportKind.BoilerReport:
                    return "Котлу";

                case ReportKind.TurbineReport:
                    return "Турбине";

                default:
                    throw new InvalidCastException("Такого параметра не предусмотрено!");
            }
        }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime CurrentDate { get { return DateTime.Now; } }

        public string ParamCaption { get; set; }

        private ReportKind m_reportKind = ReportKind.Unknown;

        public ReportKind ReportKind
        {
            get
            {
                return m_reportKind;
            }
            set
            {
                m_reportKind = value;

                switch (value)
                {
                    case ReportKind.Unknown:
                        break;

                    case ReportKind.BoilerReport:
                        GetActiveDocTurbineView().Visibility = Visibility.Collapsed;
                        GetActiveDocBoilerView().Visibility = Visibility.Visible;
                        break;

                    case ReportKind.TurbineReport:
                        GetActiveDocTurbineView().Visibility = Visibility.Visible;
                        GetActiveDocBoilerView().Visibility = Visibility.Collapsed;
                        break;
                    default:
                        break;
                }
            }
        }

        private BoilerData2View GetActiveDocBoilerView()
        {
            return GetBoilerView(GetDocument());
        }

        private static BoilerData2View GetBoilerView(FlowDocument doc)
        {
            return ((BoilerData2View)doc.FindName("boilerView"));
        }

        private static TurbineView GetTurbineView(FlowDocument doc)
        {
            return ((TurbineView)doc.FindName("turbineView"));
        }

        private TurbineData m_turbineData = null;

        public TurbineData TurbineData
        {
            get
            {
                return m_turbineData;
            }
            set
            {
                m_turbineData = value;

                GetActiveDocTurbineView().Data = value;

                UpdateCaptions();
            }
        }

        private TurbineView GetActiveDocTurbineView()
        {
            return GetTurbineView(GetDocument());
        }
    }
}