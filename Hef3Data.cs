﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Hef3MS_NET
{
	public class Hef3Data
	{
		private BoilerList m_boilers;
		
		public Hef3Data()
		{
			m_boilers = new BoilerList();
		}
		
		public BoilerList Boilers
		{
			get
			{
				return m_boilers;
			}
		}
	}
}