﻿using System;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using C1.WPF.C1Chart;
using Hef3MS.Constants;
using Hef3MS.Data;
using Hef3MS.Sensors;
using Hef3MS_NET.Data.Serialization;

namespace Hef3MS_NET
{
    /// <summary>
    /// Interaction logic for TurbineDetailView.xaml
    /// </summary>
    public partial class TurbineDetailView : UserControl
    {
        public TurbineDetailView()
        {
            this.InitializeComponent();
        }

        public TurbineData Data
        {
            get { return (TurbineData)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(TurbineData), typeof(TurbineDetailView));

        private void pressureParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Р(кгс/м2) пара";

            LoadAndViewParamsTable(
                Hef3Data2_ID.TURBINE_PRESSURE_SENSOR,
                Hef3Data2_ID.PRESSURE,
                startOfPeriodPicker.DateTimeValue,
                endOfPeriodPicker.DateTimeValue
               );
        }

        private void LoadAndViewParamsTable(int paramId, int subParamId, System.DateTime? startOfPeriod, System.DateTime? endOfPeriod)
        {
            DataTable table = ParamsTableReceiver.LoadTable(
                                paramId,
                                subParamId,
                                startOfPeriod,
                                endOfPeriod,
                //TODO: Remove unwanted param "number"
                                Data.Number,
                                (number) => Hef3Data2_ID.TURBINE_BASE + Hef3Data2_ID.TURBINE_SHIFT * (number - 1) + paramId
                                );

            this.DataContext = table;

            UpdateChart(table);
        }

        private void UpdateChart(DataTable table)
        {
            ChartData _defaultData = new ChartData();

            DataSeries ds1 = new DataSeries() { Label = "s1" };

            var items = from item in table.AsEnumerable()
                        select item.Field<float>("Value")
                            ;

            ds1.ValuesSource =
               items.ToArray()
            ;

            ds1.ConnectionStroke = new SolidColorBrush(Colors.Blue);

            _defaultData.Children.Add(ds1);

            chart.ChartType = ChartType.Line;
            chart.Data = _defaultData;
        }

        private void startStopParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Пуски\\Остановы";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_MISC_PARAMS, Hef3Data2_ID.START_STOP_COUNT);
        }

        private void LoadAndViewParamsTable(int paramId, int subParamId)
        {
            LoadAndViewParamsTable(paramId, subParamId, startOfPeriodPicker.DateTimeValue, endOfPeriodPicker.DateTimeValue);
        }

        private void workingHoursParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Отработано часов";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_MISC_PARAMS, Hef3Data2_ID.WORKING_HOURS_COUNT);
        }

        private void temperatureParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Т(°С) пара";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_TEMPERATURE_SENSOR, Hef3Data2_ID.TEMPERATURE);
        }

        private void actualFlowRateParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Актуальный расход пара";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_FLOWRATE_SENSOR, Hef3Data2_ID.FLOWRATE_ACTUAL);
        }

        private void byDeviceFlowRateParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Расход пара по датчику";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_FLOWRATE_SENSOR, Hef3Data2_ID.FLOWRATE_BYDEVICE);
        }

        private void inboundTemperatureOfCirculateWaterParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Т(°С) вх. циркуляционной воды";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_CIRCULATE_WATER_SENSOR, Hef3Data2_ID.WATER_TEMPERATURE_INCOMING);
        }

        private void outgoingTemperatureOfCirculateWaterParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Т(°С) исх. циркуляционной воды";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_CIRCULATE_WATER_SENSOR, Hef3Data2_ID.WATER_TEMPERATURE_OUTGOING);
        }

        private void temperatureOfWasteGasesParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Т(°С) отработанный пар";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_EXHAUST_STEAM_SENSOR, Hef3Data2_ID.TEMPERATURE);
        }

        private void pressureOfWasteGasesParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Р(кгс/м2) отработанного пара";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_EXHAUST_STEAM_SENSOR, Hef3Data2_ID.PRESSURE);
        }

        private void actualFlowRateOfWasteGasesParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Актуальный расход отработанного пара";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_EXHAUST_STEAM_SENSOR, Hef3Data2_ID.STEAM_FLOWRATE_ACTUAL);
        }

        private void byDeviceFlowRateOfWasteGasesParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Расход по прибору отработанного пара";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_EXHAUST_STEAM_SENSOR, Hef3Data2_ID.STEAM_FLOWRATE_BYDEVICE);
        }

        private void temperatureOfPOSteamParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Т(°С) ПО пара";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_PO_STEAM_SENSOR, Hef3Data2_ID.TEMPERATURE);
        }

        private void pressureOfPOSteamParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Р(кгс/м2) ПО пара";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_PO_STEAM_SENSOR, Hef3Data2_ID.PRESSURE);
        }

        private void actualFlowRateOfPOSteamParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Актуальный расход ПО пара";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_PO_STEAM_SENSOR, Hef3Data2_ID.STEAM_FLOWRATE_ACTUAL);
        }

        private void byDeviceOfPOSteamParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Расход ПО пара по периоду";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_PO_STEAM_SENSOR, Hef3Data2_ID.STEAM_FLOWRATE_BYDEVICE);
        }

        private void temperatureOfTOSteamParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Т(°С) ТО пара";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_TO_STEAM_SENSOR, Hef3Data2_ID.TEMPERATURE);
        }

        private void pressureOfTOSteamParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Р(кгс/м2) ТО пара";

            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_PO_STEAM_SENSOR, Hef3Data2_ID.PRESSURE);
        }

        private void actualFlowRateOfTOSteamParam_Click(object sender, RoutedEventArgs e)
        {
            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_PO_STEAM_SENSOR, Hef3Data2_ID.STEAM_FLOWRATE_ACTUAL);
        }

        private void byDeviceFlowRateOfTOSteamParam_Click(object sender, RoutedEventArgs e)
        {
            LoadAndViewParamsTable(Hef3Data2_ID.TURBINE_PO_STEAM_SENSOR, Hef3Data2_ID.STEAM_FLOWRATE_BYDEVICE);
        }

        private void printButton_Click(object sender, RoutedEventArgs e)
        {
            PreviewReport();
        }

        private void PreviewReport()
        {
            ReportWindow win = new ReportWindow();

            win.Title = String.Format("Отчёт по турбине {0}", Data.Number);

            win.ParamCaption = ParamCaption;

            win.StartDate = StartDate;
            win.EndDate = EndDate;

            win.ReportKind = ReportKind.TurbineReport;
            win.TurbineData = Data;

            if (null != DataContext)
            {
                DataTable table = (DataTable)DataContext;

                win.Data = (
                           from item in table.AsEnumerable()
                           select new SingleValueSensor
                           {
                               DateReceived = item.Field<DateTime>("DateInput"),
                               Value = item.Field<float>("Value")
                           }
                            )
                            .ToList<SingleValueSensor>()
                                ;

                win.DataContext = table;
            }

            win.ShowDialog();
        }

        private string m_paramCaption = "";

        public string ParamCaption
        {
            get
            {
                return m_paramCaption;
            }
            set
            {
                m_paramCaption = value;
                chartGroupBox.Header = String.Format("График параметра {0} за период", value);
                tableGroupBox.Header = String.Format("Таблица параметра {0} за период", value);
            }
        }

        public DateTime StartDate { get { return startOfPeriodPicker.DateTime; } }

        public DateTime EndDate { get { return endOfPeriodPicker.DateTime; } }
    }
}