﻿using System;
using System.Data;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using C1.WPF.C1Chart;
using Hef3MS.Constants;
using Hef3MS.Data;
using Hef3MS.Sensors;
using Hef3MS_NET.Data.Serialization;

namespace Hef3MS_NET
{
    /// <summary>
    /// Interaction logic for BoilerDetailedView.xaml
    /// </summary>
    public partial class BoilerDetailedView : UserControl
    {
        public BoilerData2 Data
        {
            get { return (BoilerData2)GetValue(DataProperty); }
            set { SetValue(DataProperty, value); }
        }

        public static readonly DependencyProperty DataProperty =
            DependencyProperty.Register("Data", typeof(BoilerData2), typeof(BoilerDetailedView));

        public BoilerDetailedView()
        {
            this.InitializeComponent();
        }

        private void qkbrParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Расход пара [Qkbr]";
            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_MISC_PARAMS, Hef3Data2_ID.HEAT_GROSS_VALUE);
        }

        private void LoadAndViewParamsTable(int paramId, int subParamId, System.DateTime? startOfPeriod, System.DateTime? endOfPeriod)
        {
            DataTable table = ParamsTableReceiver.LoadTable(
                                paramId,
                                subParamId,
                                startOfPeriod,
                                endOfPeriod,
                                Data.Number,
                                (number) => Hef3Data2_ID.BOILER_BASE + Hef3Data2_ID.BOILER_SHIFT * (number - 1) + paramId
                                );

            this.DataContext = table;

            UpdateChart(table);
        }

        private void UpdateChart(DataTable table)
        {
            ChartData _defaultData = new ChartData();

            DataSeries ds1 = new DataSeries() { Label = "s1" };

            var items = from item in table.AsEnumerable()
                        select item.Field<float>("Value")
                            ;

            ds1.ValuesSource =
               items.ToArray()
            ;

            ds1.ConnectionStroke = new SolidColorBrush(Colors.Blue);

            _defaultData.Children.Add(ds1);

            chart.ChartType = ChartType.Line;
            chart.Data = _defaultData;
        }

        private void LoadAndViewParamsTable(int paramId, int subParamId)
        {
            LoadAndViewParamsTable(paramId, subParamId, StartDate, endOfPeriodPicker.DateTimeValue);

            //ParamCaption = GetParamCaptionById(paramId, subParamId);
        }

        private void startStopCountParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Пуски\\Остановы";
            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_MISC_PARAMS, Hef3Data2_ID.START_STOP_COUNT);
        }

        private void workingHoursCountParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Отработано часов";

            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_MISC_PARAMS, Hef3Data2_ID.WORKING_HOURS_COUNT);
        }

        private void temperatureOfExhaustGasParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Т(°С) уходящих газов";
            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_GAS_SENSORS, Hef3Data2_ID.WASTE_GASES_TEMPERATURE);
        }

        private void airTemperatureBeforeHeaterParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Т(°С) воздуха перед нагревателем";
            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_AIR_SENSORS, Hef3Data2_ID.AIR_BEFORE_HEATER_TEMPERATURE);
        }

        private void coldAirTemperatureParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Т(°С) холодного воздуха";

            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_AIR_SENSORS, Hef3Data2_ID.COLD_AIR_TEMPERATURE);
        }

        private void hotAirTemperatureParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Т(°С) горячего воздуха";

            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_AIR_SENSORS, Hef3Data2_ID.HOT_AIR_TEMPERATURE);
        }

        private void feedWaterTemperatureParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Т(°С) питательной воды";

            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_FEED_WATER_SENSORS, Hef3Data2_ID.FEED_WATER_TEMPERATURE);
        }

        private void feedWaterPressureParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Р(кгс/м2) питательной воды";

            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_FEED_WATER_SENSORS, Hef3Data2_ID.FEED_WATER_PRESSURE);
        }

        private void feedWaterActualFlowRateParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Актуальный расход питательной воды";

            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_FEED_WATER_SENSORS, Hef3Data2_ID.FEED_WATER_FLOWRATE_ACTUAL);
        }

        private void feedWaterFlowRateByDeviceParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Расход по датчику питательной воды";

            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_FEED_WATER_SENSORS, Hef3Data2_ID.FEED_WATER_FLOWRATE_BYDEVICE);
        }

        private void steamTemperatureParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Т(°С) пара";

            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_STEAM_SENSOR, Hef3Data2_ID.STEAM_TEMPERATURE);
        }

        private void steamPressureParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Р(кгс/м2) пара";

            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_STEAM_SENSOR, Hef3Data2_ID.STEAM_PRESSURE);
        }

        private void steamActualFlowRateParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Актуальный расход пара";

            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_STEAM_SENSOR, Hef3Data2_ID.STEAM_FLOWRATE_ACTUAL);
        }

        private void steamFlowRateByDeviceParam_Click(object sender, RoutedEventArgs e)
        {
            ParamCaption = "Расход пара по датчику";

            LoadAndViewParamsTable(Hef3Data2_ID.BOILER_STEAM_SENSOR, Hef3Data2_ID.STEAM_FLOWRATE_BYDEVICE);
        }

        private void printPreviewButton_Click(object sender, RoutedEventArgs e)
        {
            PreviewReport();
        }

        private void PreviewReport()
        {
            ReportWindow win = new ReportWindow();

            win.Title = String.Format("Отчёт по котлу {0}", Data.Number);

            win.ParamCaption = ParamCaption;

            win.StartDate = StartDate;
            win.EndDate = EndDate;

            win.ReportKind = ReportKind.BoilerReport;
            win.BoilerData = Data;

            if (null != DataContext)
            {
                DataTable table = (DataTable)DataContext;

                win.Data = (
                           from item in table.AsEnumerable()
                           select new SingleValueSensor
                           {
                               DateReceived = item.Field<DateTime>("DateInput"),
                               Value = item.Field<float>("Value")
                           }
                            )
                            .ToList<SingleValueSensor>()
                                ;

                win.DataContext = table;
            }

            win.ShowDialog();
        }

        public DateTime StartDate { get { return startOfPeriodPicker.DateTimeValue ?? DateTime.Now; } }

        public DateTime EndDate { get { return endOfPeriodPicker.DateTimeValue ?? DateTime.Now; } }

        private string m_paramCaption = "";

        public string ParamCaption
        {
            get
            {
                return m_paramCaption;
            }
            set
            {
                m_paramCaption = value;

                chartGroupBox.Header = String.Format("График параметра {0} за период", ParamCaption);
                tableGroupBox.Header = String.Format("Таблица параметра {0} за период", ParamCaption);
            }
        }
    }
}